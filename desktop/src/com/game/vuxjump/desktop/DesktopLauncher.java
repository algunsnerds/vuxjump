package com.game.vuxjump.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.game.vuxjump.VuxJump;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "VuxJump-Alpha";
		config.width = 1024;
		config.height = 600;
		new LwjglApplication(new VuxJump(null), config);
	}
}
