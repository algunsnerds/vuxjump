package com.game.vuxjump;

//import com.badlogic.gdx.backends.android.AndroidApplication;

import android.app.Activity;
import android.content.pm.ActivityInfo;

public class OrientationAndroid implements OrientationChanger {
	public Activity context;
	
	public OrientationAndroid ( Activity context ) {
		this.context = context;
	}
	
	@Override
	public void changeTo ( String ori ) {
		if (ori.equals("portrait"))
			context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT);//SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		else if (ori.equals("landscape"))
			context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	}

}
