package com.game.vuxjump;

import android.annotation.TargetApi;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.view.View;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.game.vuxjump.VuxJump;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		OrientationAndroid oc = new OrientationAndroid(this); 
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		/*
		 * config.a=8 é porque tem um png com pixels translucidos (arrowButton2.png)
		 * Mas é só essa imagem que é assim.
		 */
		config.a = 8;
		View gdxView = initializeForView(new VuxJump(oc), config);
		
		final int sdk = Build.VERSION.SDK_INT;

	    if (sdk >= VERSION_CODES.HONEYCOMB)
	    {
	       preserveContext((GLSurfaceView)gdxView);
	    }
	    else
	    {
	    	GameState.instance().setReloadOnResume(true);
	    }
	    
	    setContentView(gdxView);
	}
	
	 @TargetApi(11)
    private void preserveContext(GLSurfaceView view) {
       view.setPreserveEGLContextOnPause(true);
    }
}
