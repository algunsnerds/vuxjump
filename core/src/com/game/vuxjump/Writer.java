package com.game.vuxjump;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;


public class Writer {
	
	private static Writer singleton = null;
	private Vector<Font> font = null;
	
	
	private Writer (){
		font = new Vector<Font>();
		
	}
	
	public synchronized static Writer instance (){
		if (singleton == null)
			singleton = new Writer();
		
		return singleton;
	}
	
	public void destroy (  ){
		if (singleton != null)
		{
			for (Font f: font)
			{
				f.dispose();
			}
			singleton = null;
		}
	}
	
	public class Font {
		public float scale;
		public String fontName;
		public Map<Character, TextureRegion> letter;
		public Texture texture;
		public float charSpace;
		public boolean destroyTexture;
		
		public Font (  ){
			charSpace = 0;
			scale = 1;
			fontName = "none";
			letter = new HashMap<Character, TextureRegion>();
			texture = null;
			destroyTexture = true;
		}
		
		public void setScale ( float s ){
			for (Iterator<Entry<Character, TextureRegion>> it = letter.entrySet().iterator(); it.hasNext(); )
			{
				//l.getTexture().setScale(s); //ainda não funciona
			}
		}
		
		public void dispose (  ){
			letter.clear();
			if (destroyTexture)
				texture.dispose();
		}
	}
	
	public Font getFont ( String fontName ) {
		for (Font f: font)
		{
			if (f.fontName.equals(fontName))
				return f;
		}
		
		return null;
	}
	
	public void setFontScale ( String fontName, float s ){
		for (Font f: font)
		{
			if (f.fontName.equals(fontName))
			{
				f.setScale(s);
				break;
			}
		}
	}
	
	public boolean hasFont ( String fontName ){
		for (Font f: font)
		{
			if (f.fontName.equals(fontName))
				return true;
		}
		
		return false;
	}
	
	public int getWidth ( String fontName, String str ) {
		int w = 0;
		if (hasFont(fontName))
		{
			Font f = getFont(fontName);
			for (int i = 0, k = 0; i < str.length(); i++)
			{
				k = 0;
				for (int j = i; j < str.length() && str.charAt(j) != '\n'; j++)
					k++;
				
				if (k > w)
					w = k;
			}
			
			w *= f.letter.get('a').getRegionWidth();
		}
		
		return w;
	}
	
	public Object[] getInfo ( String fontName, String str ) {
		if (hasFont(fontName))
		{
			Font f = getFont(fontName);
			int w = 0, h = 0;
			w = str.length() * f.letter.get(str.charAt(0)).getRegionWidth();
			h = f.letter.get(str.charAt(0)).getRegionHeight();
			return new Object[]{str, w, h};
		}
		
		return new Object[]{};
	}
	
	public void flip ( String fontName, boolean flipX, boolean flipY ) {
		for (Font fnt: font)
			for (Map.Entry<Character, TextureRegion> entry: fnt.letter.entrySet())
				entry.getValue().flip(flipX, flipY);;
	}
	
	private void readFont ( Font fnt, String fileFontPath ) {
		JsonReader reader = new JsonReader();
		JsonValue value, child = reader.parse(Gdx.files.internal(fileFontPath));
		
		value = child.child; 
		while(value != null){
			int[] atts = {
					value.child.get("x").asInt(),
					value.child.get("y").asInt(),
					value.child.get("w").asInt(),
					value.child.get("h").asInt()
			};

			TextureRegion region = new TextureRegion(fnt.texture, atts[0],atts[1],atts[2],atts[3]);
			region.flip(false, true);
			fnt.letter.put(value.name.charAt(0), region);
			value = value.next;			
		}
	}
	
	public void loadFont ( String fontName, String fileFontPath, String fontImagePath ){
		Font fnt = new Font();
		fnt.texture = new Texture(Gdx.files.internal(fontImagePath));
		fnt.fontName = fontName;
		
		readFont(fnt,fileFontPath);
		
		font.add(fnt);
	}
	
	/*
	 * NOTA: aqui a fnt.texture não será destruida quando chamar Font.dispose, pois ela tem que ser
	 * destruida manualmente, isso dá pra usar um assetmanager e deixar ele destruir a textura.
	*/
	public void loadFont ( String fontName, String fileFontPath, Texture texture ){
		Font fnt = new Font();
		fnt.texture = texture;
		fnt.fontName = fontName;
		fnt.destroyTexture = false;
		
		readFont(fnt,fileFontPath);
		
		font.add(fnt);
	}

	public void renderText ( SpriteBatch batch, String fontName, float x, float y, String text ){
		TextureRegion region = null;

		for (Font f: font)
		{
			if (fontName.equals(f.fontName))
			{
				float px = x, py = y;
				for (int i = 0; i < text.length(); i++)
				{
					if (text.charAt(i) == ' ')
						px += f.charSpace + f.letter.get('a').getRegionWidth();
					else if (text.charAt(i) == '\n')
					{
						px = x;
						py +=  f.letter.get('a').getRegionHeight();
						//text
					}
					
					for (Map.Entry<Character,TextureRegion> entry: f.letter.entrySet())
					{
						if (entry.getKey() == (Character)text.charAt(i))
						{
							region = entry.getValue();
							batch.draw(region, px, py, region.getRegionWidth() * f.scale, region.getRegionHeight() * f.scale);
							px += f.charSpace + region.getRegionWidth() * f.scale;
						}
					}
				}
				break;
			}
		}
	}
	
	public void renderText ( SpriteBatch batch, String fontName, Color fontColor, float x, float y, String text ){
		TextureRegion region = null;
		
		for (Font f: font)
		{
			if (fontName.equals(f.fontName))
			{
				float px = x, py = y;
				for (int i = 0; i < text.length(); i++)
				{
					if (text.charAt(i) == ' ')
						px += f.charSpace + f.letter.get('a').getRegionWidth();
					else if (text.charAt(i) == '\n')
					{
						px = x;
						py +=  f.letter.get('a').getRegionHeight();
						//text
					}
					
					for (Map.Entry<Character,TextureRegion> entry: f.letter.entrySet())
					{
						if (entry.getKey() == (Character)text.charAt(i))
						{
							region = entry.getValue();
							Color colorBak = batch.getColor(); 
							batch.setColor(fontColor);//
							batch.draw(region, px, py, region.getRegionWidth() * f.scale, region.getRegionHeight() * f.scale);
							batch.setColor(colorBak);
							px += f.charSpace + region.getRegionWidth() * f.scale;
						}
					}
				}
				break;
			}
		}
	}
	
}
