package com.game.vuxjump;

import com.badlogic.gdx.math.Vector2;

public class Rect {
	public int x, y;
	public int w, h;
	
	public Rect () {
		x = y = 0;
		w = h = 0;
	}
	
	public Rect ( int mx, int my, int mw, int mh ) {
		x = mx;
		y = my;
		w = mw;
		h = mh;
	}
	
	public Rect ( Rect r ){
		x = r.x;
		y = r.y;
		w = r.w;
		h = r.h;
	}
	
	public Rect set ( Rect r ){
		x = r.x;
		y = r.y;
		w = r.w;
		h = r.h;
		
		return this;
	}
	
	public void set ( Vector2 v ){
		x = (int)v.x;
		y = (int)v.y;
	}
	
	public void setScale ( float s ) {
		w = (int)(s * w);
		h = (int)(s * h);
	}
	
	public boolean boundingbox ( Rect b )
	{
		if (x > b.x + (b.w - 1))	return false;
		if (x + w < b.x)			return false;

		if (y > b.y + (b.h - 1))	return false;
		if (y + h < b.y)			return false;

		return true;
	}

	public boolean pointbox ( Vector2 p )
	{
		//p.toInt();
		
		if (p.x > x + (w - 1))	return false;
		if (p.x < x)			return false;

		if (p.y > y + (h - 1))	return false;
		if (p.y < y)			return false;

		return true;
	}

	// verifica se "a" está completamente dentro de "b", mas não o contrário
	public boolean inside ( Rect b )
	{
		if (x + w - 1 > b.x + (b.w - 1))	return false;
		if (x < b.x)						return false;

		if (y + (h - 1) > b.y + (b.h - 1))	return false;
		if (y < b.y)						return false;

		return true;
	}
	
	public void zero (  ){
		x = y = 0;
		w = h = 0;
	}

	public void set ( int x, int y, int w, int h ) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}
}
