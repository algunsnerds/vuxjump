package com.game.vuxjump.screen;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector.GestureListener;

public interface GameScreen extends GestureListener, Screen, InputProcessor {
	//chamado ao trocar de screen
	public void onChange (  );
}
