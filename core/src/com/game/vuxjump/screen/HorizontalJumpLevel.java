package com.game.vuxjump.screen;

import java.util.Vector;



import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.game.vuxjump.ScrolledBackground;
import com.game.vuxjump.StaticBackground;
import com.game.vuxjump.GameState;
import com.game.vuxjump.Writer;
import com.game.vuxjump.elements.Fish;
import com.game.vuxjump.elements.Platform;
import com.game.vuxjump.elements.PlatformGroup;
import com.game.vuxjump.elements.Player;
import com.game.vuxjump.elements.PlayerTarget;
import com.game.vuxjump.elements.Treak;
import com.game.vuxjump.gui.LevelGui;
import com.game.vuxjump.hud.LevelHUD;

/*
 * Todas pessoas tentam jogar pensando que o jogo é de arrastar!
 * 
 * 
 */

public class HorizontalJumpLevel extends JumpLevel {
	public static final int LEVEL = 0;
	public static final int MENUBG = 1;
	
	protected ScrolledBackground background;
	protected PlayerTarget playerPos;
	public int mode = LEVEL;
	
	protected String platTexture = "null";
	protected String bgPath = "null"; // path para o background
	protected String[] groupsPath;
	protected int srcW, srcH;
	
	private boolean endLevel = false;
	
	public HorizontalJumpLevel ( String bgPath, String platTexture, String[] groupsPath, int srcW, int srcH ) {
		this.bgPath = bgPath;
		this.platTexture = platTexture;
		this.groupsPath = groupsPath;
		scrolled = false;
		this.srcW = srcW;
		this.srcH = srcH;
	}
	
	public HorizontalJumpLevel ( String bgPath, String platTexture, String[] groupsPath, boolean scroll ) {
		this.bgPath = bgPath;
		this.platTexture = platTexture;
		this.groupsPath = groupsPath;
		scrolled = scroll;
		srcW = 1024;
		srcH = 512;
	}
	
	
	
	@Override
	public void show() {
		// chama show() para construir alguns objetos comuns
		super.show();
		
		if (inited == 1)
		{
			// libera memoria anterior
			gui.dispose();
			gui = new LevelGui(this);
			gui.setArrows(true);
			reset();
			return;
		}
		else if (inited == 2)
		{
			Gdx.app.log("GreenLevel","inited == 2 era pra recarregar as imagens no android API < 11");
			return;
		}
		//GameState.setOrientation("portrait");
		inited = 1;
		
		
		
		gui = new LevelGui(this);
		gui.setArrows(true);
		
		if (scrolled)
			scrollBG = new ScrolledBackground(camera, bgPath, srcW, srcH);
		else
			staticBG = new StaticBackground(camera, bgPath, srcW, srcH);
		
		platformTexture = new Texture(Gdx.files.internal(platTexture));
		/*
		 * Cada grupo deve estar com ids indo de 0 a N
		 */
		/*
		 * Plataformas devem ser posicionadas da mais a esquerda para a mais a direita
		 * Cada grupo deve ter dimensões maior que a de landscape, h>=600 e w>=1024
		 */
		for (int i = 0; i < groupsPath.length; i++)
		{
			baseGroup.add(new PlatformGroup(camera,PlatformGroup.HORIZONTAL,i));
			baseGroup.get(i).loadPlatforms(camera,platformTexture,groupsPath[i]);
		}
		
		
		
		/*
		* Tem que ser um seguido de outro diferente
		* Tipo, groou1, depois group2, depois group1, depois group2, etc.
		* RESOLVIDO]Fazer um método para misturar aleatoriamente os grupos
		* 
		* IMPORTANTE:
		* -Tem que ser de acordo com a paridade da quantidade de grupos carregados,
		* então se for uma qtde par groupsSize tem que ser par, 
		* senão se for impar, groupsSize tem que ser impar.
		* groupsSize tem que ser maior que 3 pra fase horizontal
		*/
		groupsSize = groupsPath.length;
		//if (groupsSize % 2 != 0)
		//	groupsSize++;
		mixGroups(groupsSize);
		
		Gdx.app.log("HorizontalJumpLevel","groupsSize = "+groupsSize);
		
		groups.get(0).setPosition(new Vector2());
		groups.get(1).setPosition(new Vector2(groups.get(0).getPosition().x + groups.get(0).getDim().w, groups.get(0).getPosition().y));
		
		player = new Player(null, camera);
		player.addPlatforms(groups.get(0).getPlatforms());
		player.addPlatforms(groups.get(1).getPlatforms());
		
		// pega a melhor plataforma que é a que tem posição.y > 300
		// pois o gamer deve ver a plataforma
		Platform base = groups.get(0).getPlatform(3);
		int i = 0;
		for (Platform p: groups.get(0).getPlatforms())
		{
			if (i >= 2 && p.getPosition().y > 300)
			{
				base = p;
				break;
			}
			i++;
		}
		player.reset(base);
		player.setBoundary(camera);
		
		addTreaks();
		/*
		i = 0;
		for (int j = 0; j < getCurrGroup().getPlatforms().size(); j++)
		{
			// terceira plataforma onde fica o player
			if (j != 3)
			{
				treaks.add(new Treak(null, player));
				treaks.get(i).addPlatforms(getCurrGroup().getPlatforms());
				treaks.get(i).setPosition(getCurrGroup().getPlatform(j));
				i++;
			}
		}
		*/
		playerPos = new PlayerTarget(player);
		LevelHUD.instance().setCamera(camera);
		LevelHUD.instance().setPlayer(player);
		Writer.instance().loadFont("font50", "font50.json", "font50.png");
	}
	
	
	@Override
	public void render(float delta) {
		
		if(!gui.isPaused() && endLevel == false){
			for (Treak treak: treaks)
				treak.update();
			player.update();
			playerPos.update();
			getCurrGroup().update();
			getNextGroup().update();
			
			
			if (updateGroups(JumpLevel.HORIZONTAL))
				addTreaks();
			
			// NÃO converter para int pois pode causar pulos na visualização
			// funciona mal mas diminui bastante.
			if (player.getPosition().x > camera.position.x)
			{
				camera.position.set(player.getPosition().x, camera.position.y, 0);
				camera.update();
				if (scrolled)
					scrollBG.update();
				else
					staticBG.update();
				player.setBoundary(camera);
				LevelHUD.instance().update();
			}
			
			if (player.getState() == Player.DIED_STATE)
			{
				endLevel = true;
			}
		}
		
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		batch = GameState.instance().getBatch();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			if (scrolled)
				scrollBG.draw(batch, camera);
			else
				staticBG.draw(batch, camera);
			
			getCurrGroup().draw(batch, camera);
			getNextGroup().draw(batch,camera);
			
			for (Treak treak: treaks)
				treak.draw(batch, camera);
			player.draw(batch, camera);
			playerPos.draw(batch, camera);
			
			LevelHUD.instance().draw(batch, camera);
			//Writer.instance().renderText(batch, "font50", (int)(camera.position.x - camera.viewportWidth/2), 0, "FPS="+Gdx.graphics.getFramesPerSecond());
			Writer.instance().renderText(batch, "font50", (int)(camera.position.x - camera.viewportWidth/2), 0, "currGroup="+getCurrGroupCount());
			
			if (endLevel)
			{
				
			}
			
		batch.end();
		
		gui.draw();
		
	}
	
	@Override 
	public void reload (  ) {
		inited = 2;
	}
	
	// taxa máxima para aparecer uma foca, em percentagem
	float treakChance = 0.7f;
	// grupo de plataformas dos treaks
	int treaksGroup = -1;
	
	public void setTreakChance ( float c ) {
		treakChance = c;
	}
	
	
	/*
	 * Adiciona focas treaks no grupo que é o próximo
	 */
	void addTreaks (  ) {
		
		
		PlatformGroup next = getNextGroup();
		
		if (treaksGroup == -1)
			treaksGroup = next.getID();
		
		if (treaksGroup != getCurrGroup().getID())
			treaksGroup = next.getID();
		
		Vector<Platform> platforms = next.getPlatforms();
		int i = 0, j = 0;
		
		/*
		if (treaksGroup == getCurrGroup().getID())
			i = treaks.size();
		*/
		final Vector<Treak> treaksToUse = new Vector<Treak>();
		for (Treak treak: treaks)
			if (treak.getPosition().x < camera.position.x && treak.inPlayerView() == false)
				treaksToUse.add(treak);
							
		for (Platform p: platforms)
		{
			if (Math.random() <= treakChance)
			{
				if (i >= treaks.size() || treaks.size() == 0)
				{
					treaks.add(new Treak(null,player));
					treaks.get(i).clearPlatforms();
					for (PlatformGroup group: baseGroup)
						treaks.get(i).addPlatforms(group.getPlatforms());
					treaks.get(i).reset(p);
				}
				else if (j < treaksToUse.size() && treaksToUse.size() > 0)
				{
					for (PlatformGroup group: baseGroup)
						treaksToUse.get(i).addPlatforms(group.getPlatforms());
					treaksToUse.get(j).reset(p);
					j++;
				}
				else
				{
					treaks.add(new Treak(null,player));
					for (PlatformGroup group: baseGroup)
						treaks.lastElement().addPlatforms(group.getPlatforms());
					treaks.lastElement().reset(p);
				}
				
				i++;
			}
		}
		
		Gdx.app.log("JumpLevel","treaks.size="+treaks.size());
	}
	
	// para o main menu
	public void init ( OrthographicCamera camera, int mode ) {
		this.mode = mode;
		this.camera = camera;
		scrolled = true;
		scrollBG = new ScrolledBackground(camera, bgPath, 1024, 512); 
		platformTexture = new Texture(Gdx.files.internal(platTexture));
		
		for (int i = 0; i < 2; i++)
		{
			baseGroup.add(new PlatformGroup(camera,PlatformGroup.HORIZONTAL,i));
		}
		
		baseGroup.get(0).loadPlatforms(camera,platformTexture,groupsPath[0]);
		baseGroup.get(1).loadPlatforms(camera,platformTexture,groupsPath[0]);
		groupsSize = 4;
		mixGroups(groupsSize);
		groups.get(0).setPosition(new Vector2());
		groups.get(1).setPosition(new Vector2(groups.get(0).getPosition().x + groups.get(0).getDim().w, groups.get(0).getPosition().y));
	}

	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		scrollBG.update();
		getCurrGroup().update();
		getNextGroup().update();
		
		scrollBG.draw(batch, camera);
		getCurrGroup().draw(batch, camera);
		getNextGroup().draw(batch,camera);
	}
	
	@Override
	public void dispose() {
		super.dispose();
		inited = 0;
		
		if (scrolled)
			scrollBG.dispose();
		else
			staticBG.dispose();
		
		platformTexture.dispose();
		
		if (mode != MENUBG)
		{
			player.dispose();
			playerPos.dispose();
			gui.dispose();
		}
		
		for (Treak treak: treaks)
			treak.dispose();
	}
	
	public void reset (  ) {
		mixGroups(groupsSize);
		
		groups.get(0).setPosition(new Vector2());
		groups.get(1).setPosition(new Vector2(groups.get(0).getPosition().x + groups.get(0).getDim().w, groups.get(0).getPosition().y));		
		
		player.clearPlatforms();
		
		player.addPlatforms(groups.get(0).getPlatforms());
		player.addPlatforms(groups.get(1).getPlatforms());
		//setCurrGroup(-1);
		nextGroup = 0;
		setCurrGroup();
		getCurrGroup().unreset();
		getCurrGroup().reset();
		getNextGroup().unreset();
		getNextGroup().reset();
		
		//float x = getCurrGroup().getPlatform(3).getPosition().x + getCurrGroup().getPlatform(3).getCollRect().w/2;
		//Gdx.app.log("GreenLevel","plat.x+w/2="+x);
		player.reset(getCurrGroup().getPlatform(3));
		
		addTreaks();
		/*
		for (int j = 0, i = 0; j < getCurrGroup().getPlatforms().size(); j++)
		{
			// terceira plataforma onde fica o player
			if (j != 3)
			{
				treaks.add(new Treak(null, player));
				treaks.get(i).addPlatforms(getCurrGroup().getPlatforms());
				treaks.get(i).setPosition(getCurrGroup().getPlatform(j));
				i++;
			}
		}
		*/
		
		Gdx.app.log("GreenLevel","Resetar as focas também");
		Gdx.app.log("GreenLevel","player.x="+player.getPosition().x);
		
		camera.position.x = camera.viewportWidth/2;
		camera.update();
		if (scrolled)
			scrollBG.update();
		else
			staticBG.update();
		player.setBoundary(camera);
		LevelHUD.instance().reset();
		endLevel = false;
	}
}
