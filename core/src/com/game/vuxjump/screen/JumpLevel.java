package com.game.vuxjump.screen;

import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.game.vuxjump.GameState;
import com.game.vuxjump.Rect;
import com.game.vuxjump.ScrolledBackground;
import com.game.vuxjump.StaticBackground;
import com.game.vuxjump.Writer;
import com.game.vuxjump.elements.PlatformGroup;
import com.game.vuxjump.elements.Player;
import com.game.vuxjump.elements.Treak;
import com.game.vuxjump.gui.LevelGui;
import com.game.vuxjump.hud.LevelHUD;

public class JumpLevel implements GameScreen { 
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	
	protected SpriteBatch batch;
	protected OrthographicCamera camera;
	protected Viewport viewport;
	protected LevelGui gui;
	protected int inited = 0;
	
	protected Player player;
	protected Vector<Treak> treaks;
	
	protected Texture platformTexture;
	protected int currGroup, nextGroup;
	protected Vector<PlatformGroup> groups, baseGroup;
	
	// futuras modificações, juntar Ice level com o green level
	protected boolean scrolled = false;
	protected StaticBackground staticBG;
	protected ScrolledBackground scrollBG;
	protected int groupsSize;
	protected Vector2 pos = new Vector2();
	
	public JumpLevel (  ) {
		groups = new Vector<PlatformGroup>();
		baseGroup = new Vector<PlatformGroup>();
		treaks = new Vector<Treak>();
		
		nextGroup = 0;
		setCurrGroup();
		
		/*/
		 * Para ter plataformas de fase horizontal, recomendo que a primeira, seja a mais
		 * a esquerda do grupo.
		 * Para as de fase vertical a primeira deve ser a plataforma mais baixa, ou com pos.y
		 * perto de 0. Pois nos dois casos acima, a primeira plataforma será a que posiciona
		 * o player na fase.
		 */
		if (GameState.getOrientationValue().equals("portrait"))
		{
			camera = new OrthographicCamera(600,1024);
			camera.setToOrtho(true, 600,1024);
			viewport = new StretchViewport(600,1024, camera);
		}
		else
		{
			camera = new OrthographicCamera(1024, 600);
			camera.setToOrtho(true, 1024, 600);
			viewport = new StretchViewport(1024,600, camera);
		}
	}
	
	public void setPosition ( Vector2 p ) {
		pos.set(p);
	}
	
	@Override
	public void show() {
		
	}
	
	@Override
	public void render(float delta) {

		if(!gui.isPaused()){
			for (Treak treak: treaks)
				treak.update();
			player.update();
			player.config.velStartJump = -40;
			getCurrGroup().update();
			getNextGroup().update();
			
			// não converter para int pois pode causar pulos na visualização 
			if (player.getPosition().y <= camera.position.y - camera.viewportHeight*0.3f){
				camera.position.set(camera.position.x, player.getPosition().y + camera.viewportHeight*0.3f, 0);
				camera.update();
				player.setBoundary(camera);
				LevelHUD.instance().update();
				if (scrolled == true)
					scrollBG.update();
				else
					staticBG.update();
			}
			
			
			updateGroups(JumpLevel.VERTICAL);
		}
		
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		batch = GameState.instance().getBatch();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			if(gui.isPaused()){batch.setColor(1,1,1,0.5f);}else{batch.setColor(1,1,1,1f);}
			if (scrolled == true)
				scrollBG.draw(batch, camera);
			else
				staticBG.draw(batch,camera);
			getCurrGroup().draw(batch, camera);
			getNextGroup().draw(batch,camera);
			
			for (Treak treak: treaks)
				treak.draw(batch, camera);
			player.draw(batch, camera);
			
			LevelHUD.instance().draw(batch, camera);
			Writer.instance().renderText(batch, "font50", (int)(camera.position.x - camera.viewportWidth/2), camera.position.y - camera.viewportHeight/2, "FPS="+Gdx.graphics.getFramesPerSecond());
			Writer.instance().renderText(batch, "font50", (int)(camera.position.x - camera.viewportWidth/2), camera.position.y - camera.viewportHeight/2 + 64, "CurrGroup = "+getCurrGroupCount());
			batch.setColor(0,0,0,1f);
		batch.end();
		
		gui.draw();
	}
	

	// falta testar, para jogo de pular vertical
	public boolean updateGroups ( int mode ) {
		//if (player.getBoundary().boundingbox(groups.get(currGroup).getDim()) == true)
			//return;
		
		
		
		
		if (player.getBoundary().boundingbox(groups.get(currGroup).getDim()) == true)
			return false;
		//Gdx.app.log("JumpLevel","Chegou aqui");
		int oldGroup = currGroup; 
		player.remPlatforms(groups.get(oldGroup).getPlatforms());
		groups.get(oldGroup).unreset();
		groups.get(oldGroup).reset();
		
		pos.set(getCurrGroup().getPosition());
		if (mode == JumpLevel.HORIZONTAL)
		{
			pos.x += groups.get(oldGroup).getDim().w;
			pos.y = 0;
		}
		else if (mode == JumpLevel.VERTICAL)
		{
			pos.x = 0;
			//pos.y = pos.y - groups.get(currGroup).getDim().h;
		}
		
		//setCurrGroup(getCurrGroupCount() + 1);
		setCurrGroup();
		
		Gdx.app.log("JumpLevel","estadoGrupo = old="+oldGroup+",curr="+currGroup+",next="+nextGroup);
		
		//getCurrGroup().setPosition(pos);
		
		Vector2 p = new Vector2(pos.x, pos.y - getCurrGroup().getDim().h);
		if (mode == HORIZONTAL)
		{
			p.y = 0;
		}
		else
		{
			p.x = 0;
			//p.y -= getCurrGroup().getDim().h;
		}
		// currGroup aqui é o antigo nextGroup
		if (player.getBoundary().boundingbox(groups.get(currGroup).getDim()) == false)
			groups.get(currGroup).setPosition(p);
		//groups.get(currGroup).reset();
		
		
		
		p.set(getCurrGroup().getPosition().x + getCurrGroup().getDim().w, getCurrGroup().getPosition().y - getNextGroup().getDim().h);
		if (mode == HORIZONTAL)
		{
			p.y = 0;
		}
		else
		{
			p.x = 0;
		}
		
		Gdx.app.log("JumpLevel","p.x="+p.x+" p.y="+p.y);
		
		groups.get(nextGroup).setPosition(p);
		if (player.getBoundary().boundingbox(groups.get(nextGroup).getDim()) == false)
		{
			groups.get(nextGroup).unreset();
			groups.get(nextGroup).reset();
		}
		player.clearPlatforms();
		player.addPlatforms(groups.get(currGroup).getPlatforms());
		player.addPlatforms(groups.get(nextGroup).getPlatforms());
		
		
		return true;
		
		/*
		 * Quando curr.id = 0 e next.id = 2, não há nehum draw do next, não aparece plataformas
		 * Quando curr.id = 1 e next.id = 0, há draw do next, mas quando toca a linha que move a camera
		 * então ele buga geral. Acontece o mesmo com curr.id = 0 e next.id = 1
		 * 
		 * Quando curr.id = 2 e next.id = 0, ficam todas plataformas dos dois grupos juntas na tela e 
		 * quando toca linha que move camera buga geral. 
		 */
	}
	
	Rect cameraRect = new Rect();
	public void updateLevelBG (  ) {
		cameraRect.set((int)(camera.position.x - camera.viewportWidth/2), (int)(camera.position.y - camera.viewportHeight/2), (int)camera.viewportHeight,(int)camera.viewportHeight);
		if (cameraRect.boundingbox(groups.get(currGroup).getDim()) == true)
			return;
		//setCurrGroup(getCurrGroupCount() + 1);
		setCurrGroup();
	}
	
	public PlatformGroup getCurrGroup (  ) {
		if (currGroup > -1 && groups.size() > 0)
			return groups.get(currGroup);
		Gdx.app.log("JumpLevel", "fora do limite, currGroup="+currGroup);
		return null;
	}
	
	public PlatformGroup getNextGroup (  ) {
		if (nextGroup > -1 && groups.size() > 0)
			return groups.get(nextGroup);
		Gdx.app.log("JumpLevel", "fora do limite, nextGroup="+nextGroup);
		return null;
	}
	
	public int getCurrGroupCount (  ) {
		return currGroup;
	}
	
	public int getNextGroupCount (  ) {
		return nextGroup;
	}
	
	public void setCurrGroup (  ) {
		currGroup = nextGroup;
		nextGroup = currGroup + 1;
		
		{
			if (currGroup == groups.size() - 1)
			{
				currGroup = groups.size() - 1;
				nextGroup = 0;
			}
			
			if (currGroup == groups.size())
			{
				currGroup = 0;
				nextGroup = 1;
			}
		}
	}
	/*
	 * O primeiro do grupo não pode ser o último, além de size ser > 3, senão buga tudo.
	 * Além disso tem que ter no minimo 2 grupos diferentes em baseGroups.
	 * É por causa desses bugs que se deve ter dois grupos diferentes em cada ponta, pois
	 * eles são unicos aqui e cada um será reposicionado conforme avança na fase.
	 * Como devem ser um grupo X seguido de um Y, não pode ter 3 grupos com 2 bases, pois,
	 * de todo modo dá um mesmo grupo seguido dele mesmo e buga o jogo.
	 */
	public void mixGroups ( int size ) {
		if (baseGroup.size() == 0 || size <= 3 || baseGroup.size() < 2)
		{
			Gdx.app.log("JumpLevel","Poucos grupos base em baseGroup, ou size é <= três");
			throw new RuntimeException();
		}
		int rnd = (int)(baseGroup.size()*Math.random());
		groups.clear();
		groups.add(baseGroup.get(rnd));
		int gid;
		PlatformGroup next;
		for (int i = 1; i < size - 1; i++)
		{
			gid = groups.get(i - 1).getID();
			
			do
			{
				next = baseGroup.get((int)(baseGroup.size()*Math.random()));
			} while (gid == next.getID());
			
			groups.add(next);
		}
		
		gid = groups.get(0).getID();
		do
		{
			next = baseGroup.get((int)(baseGroup.size()*Math.random()));
		} while (gid == next.getID());
		// adiciona o ultimo grupo que é diferente do primeiro adicionado
		groups.add(next);
	}
	
	public Player getPlayer (  ) {
		return player;
	}
	
	public OrthographicCamera getCamera (  ) {
		return camera;
	}

	@Override
	public void resize(int width, int height) {
		gui.resize(width,height);
		viewport.update(width,height);
	}
	
	@Override
	public boolean keyDown(int keycode) {
		return player.keyboardInput(keycode, true);
	}
	
	boolean screen = false;
	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK)
		{
			GameState.changeTo("levelSelector");
			return true;
		}
		
		return player.keyboardInput(keycode, false);	
	}
	
	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return player.touchInput(screenX, screenY, pointer, button, true);
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return player.touchInput(screenX, screenY, pointer, button, false);
	}
	
	@Override
	public void dispose() {
		for (PlatformGroup g: baseGroup)
			g.dispose();
	}
	
	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}

	public void reload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onChange() {
		// TODO Auto-generated method stub
		
	}
}
