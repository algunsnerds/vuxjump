package com.game.vuxjump.screen;

import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.game.vuxjump.ScrolledBackground;
import com.game.vuxjump.GameState;
import com.game.vuxjump.StaticBackground;
import com.game.vuxjump.Writer;
import com.game.vuxjump.elements.Platform;
import com.game.vuxjump.elements.PlatformGroup;
import com.game.vuxjump.elements.Player;
import com.game.vuxjump.elements.PlayerTarget;
import com.game.vuxjump.elements.Treak;
import com.game.vuxjump.gui.LevelGui;
import com.game.vuxjump.hud.LevelHUD;

public class VerticalJumpLevel extends JumpLevel {
	protected StaticBackground background;
	
	protected String platTexture = "null";
	protected String bgPath = "null"; // path para o background
	protected String[] groupsPath;
	
	public VerticalJumpLevel ( String bgPath, String platTexture, String[] groupsPath, boolean scroll ) {
		this.bgPath = bgPath;
		this.platTexture = platTexture;
		this.groupsPath = groupsPath;
		scrolled = scroll;
	}
	
	@Override
	public void show() {
		// chama show() para construir alguns objetos comuns
		super.show();
		
		if (inited == 1)
		{
			// libera memoria anterior
			gui.dispose();
			gui = new LevelGui(this);
			gui.setArrows(true);
			reset();
			return;
		}
		else if (inited == 2)
		{
			Gdx.app.log("IceLevel","inited == 2 era pra recarregar as imagens no android API < 11");
			return;
		}
		
		//GameState.setOrientation("portrait");
		inited = 1;
		
		
		
		gui = new LevelGui(this);
		gui.setArrows(true);
		
		background = new StaticBackground(camera, bgPath, 600, 1024); 
		
		platformTexture = new Texture(Gdx.files.internal(platTexture));
		for (int i = 0; i < groupsPath.length; i++)
		{
			baseGroup.add(new PlatformGroup(camera,PlatformGroup.VERTICAL,i));
			/*
			 * Plataformas devem ser posicionadas da mais baixa para a mais alta
			 * Cada grupo deve ter dimensões maior que a de portrait, h>=1024 e w>=600
			 */
			baseGroup.get(i).loadPlatforms(camera,platformTexture,groupsPath[i]);
		}
		groups = new Vector<PlatformGroup>();
		
		player = new Player(null, camera);
		player.config.velStartJump = -20;
		
		/*
		* Tem que ser um seguido de outro diferente
		* Tipo, groou1, depois group2, depois group1, depois group2, etc.
		* Fazer um método para misturar aleatoriamente os grupos
		* 
		* IMPORTANTE:
		* -Tem que ser de acordo com a paridade da quantidade de grupos carregados,
		* então se for uma qtde par groupsSize tem que ser par, 
		* senão se for impar, groupsSize tem que ser impar.
		*/
		groupsSize = groupsPath.length * 2;
		if (groupsPath.length % 2 != 0)
			groupsSize++;
		reset();
		
		LevelHUD.instance().setCamera(camera);
		LevelHUD.instance().setPlayer(player);
		Writer.instance().loadFont("font50", "font50.json", "font50.png");
	}
	
	
	@Override
	public void render(float delta) {
		
		if(!gui.isPaused()){
			for (Treak treak: treaks)
				treak.update();
			player.update();
			//player.config.velStartJump = -40;
			getCurrGroup().update();
			getNextGroup().update();
			
			// não converter para int pois pode causar pulos na visualização 
			if ((int)player.getPosition().y < (int)(camera.position.y - camera.viewportHeight*0.05f)){
				camera.position.set(camera.position.x, player.getPosition().y + camera.viewportHeight*0.05f, 0);
				camera.update();
				player.setBoundary(camera);
				LevelHUD.instance().update();
			}
			
			background.update();
			updateGroups(JumpLevel.VERTICAL);
		}
		
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		batch = GameState.instance().getBatch();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			//if(gui.isPaused()){batch.setColor(1,1,1,0.5f);}else{batch.setColor(1,1,1,1f);}
			background.draw(batch, camera);
			
			getNextGroup().draw(batch,camera);
			getCurrGroup().draw(batch, camera);
			
			for (Treak treak: treaks)
				treak.draw(batch, camera);
			player.draw(batch, camera);
			
			LevelHUD.instance().draw(batch, camera);
			Writer.instance().renderText(batch, "font50", (int)(camera.position.x - camera.viewportWidth/2), camera.position.y - camera.viewportHeight/2, "FPS="+Gdx.graphics.getFramesPerSecond());
			Writer.instance().renderText(batch, "font50", (int)(camera.position.x - camera.viewportWidth/2), camera.position.y - camera.viewportHeight/2 + 64, "CurrGroup="+getCurrGroupCount()+"\n ID="+getCurrGroup().getID()+"\nNextGroup="+getNextGroupCount()+"\n ID="+getNextGroup().getID());

			Writer.instance().renderText(batch,"font50",0,(camera.position.y - camera.viewportHeight*0.2f), "----------------------------");
			batch.setColor(0,0,0,1f);
		batch.end();
		
		gui.draw();
		
	}

	
	@Override
	public void dispose() {
		super.dispose();
		inited = 0;
		background.dispose();
		platformTexture.dispose();
		player.dispose();
		Gdx.app.log("IceLevel linha144","aqui não pode liberar o greenlevelHUD");
		//GreenLevelHUD.instance().dispose();
		gui.dispose();
	}
	
	public void reset (  ) {
		mixGroups(groupsSize);
		
		nextGroup = 0;
		setCurrGroup();
		getCurrGroup().unreset();
		getCurrGroup().reset();
		getNextGroup().unreset();
		getNextGroup().reset();
		
		getCurrGroup().setPosition(new Vector2());
		getNextGroup().setPosition(new Vector2(getCurrGroup().getPosition().x, getCurrGroup().getPosition().y - getNextGroup().getDim().h));
		player.addPlatforms(getCurrGroup().getPlatforms());
		player.addPlatforms(getNextGroup().getPlatforms());
		//setCurrGroup(-1);
		
		
		//updateGroups(JumpLevel.VERTICAL);
		
		player.reset(getCurrGroup().getPlatform(0));
		camera.position.set(camera.viewportWidth/2,player.getPosition().y - camera.viewportHeight*0.2f,0);
		camera.update();
		Gdx.app.log("IceLevel","pos.y="+player.getPosition().y);
		player.setBoundary(camera);

		//treak = new Treak(null, player);
		//treak.addPlatforms(getCurrGroup().getPlatforms());
		//Platform large = getCurrGroup().getPlatform(Platform.LARGE_GREEN);
		//treak.setPosition(large);
	}
}
