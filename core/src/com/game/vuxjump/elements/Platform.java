package com.game.vuxjump.elements;

import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;

public class Platform extends StateMachine {
	/*
	 * LITTLE tem 3*64 de largura
	 * MEDIUM tem (4ou5)*64 de largura
	 * LARGE tem 7*64 de largura
	 */
	public static final int LITTLE_GREEN = 0;
	public static final int MEDIUM_GREEN = 1;
	public static final int LARGE_GREEN = 2;
	
	public static final int MEDIUM_0_ICE = 3;
	public static final int MEDIUM_1_ICE = 4;
	public static final int MEDIUM_2_ICE = 5;
	public static final int LARGE_ICE = 6;
	
	public static final int MEDIUM_0_FUTURE = 7;
	public static final int MEDIUM_1_FUTURE = 8;
	public static final int MEDIUM_2_FUTURE = 9;
	public static final int LARGE_0_FUTURE = 10;
	public static final int LARGE_1_FUTURE = 11;
	int type = LITTLE_GREEN;
	
	public static final int ON_GAME = 0;
	public static final int FALLING = 1;
	
	protected float collY;//soma para posição de collRect
	protected int touchs = 0;
	protected Rect collRect;
	protected Rect source;
	protected Rect dest;
	protected TextureRegion platformRegion;
	protected TextureRegion colision;
	protected String name;
	protected Vector<Fish> fishes;
	protected OrthographicCamera camera;
	protected Vector2 initPos;
	
	int shakes = 3, shakeCount = 0;
	boolean shake = false;
	
	public Platform ( OrthographicCamera camera, Texture texture, int type, int fishType, int x, int y ) {
		collRect = new Rect();
		dest = new Rect();
		source = new Rect();
		fishes = new Vector<Fish>();
		this.type = type;
		
		int qtde = 3;
		
		
		switch (this.type)
		{
			case LITTLE_GREEN:
				source.x = 0*64;
				source.y = 2*64;
				source.w = 3*64;
				source.h = 2*64;
				name = "littleGreen";
				qtde = 3;
				break;
			case MEDIUM_GREEN:
				source.x = 3*64;
				source.y = 2*64;
				source.w = 4*64;
				source.h = 2*64;
				name = "mediumGreen";
				qtde = 4;
				break;
			case LARGE_GREEN:
				source.x = 0*64;
				source.y = 0*64;
				source.w = 7*64;
				source.h = 2*64;
				name = "largeGreen";
				qtde = 7;
				break;
			case MEDIUM_0_ICE:
				source.x = 0*64;
				source.y = 2*64;
				source.w = 4*64;
				source.h = 3*64;
				name = "medium0Ice";
				qtde = 4;
				break;
			case MEDIUM_1_ICE:
				source.x = 4*64;
				source.y = 2*64;
				source.w = 4*64;
				source.h = 2*64;
				name = "medium1Ice";
				qtde = 4;
				break;
			case MEDIUM_2_ICE:
				source.x = 4*64;
				source.y = 4*64;
				source.w = 4*64;
				source.h = 3*64;
				name = "medium2Ice";
				qtde = 4;
				break;
			case LARGE_ICE:
				source.x = 0*64;
				source.y = 0*64;
				source.w = 7*64;
				source.h = 2*64;
				name = "largeIce";
				qtde = 7;
				break;
			/// FUTURE ///////////////////
			case MEDIUM_0_FUTURE:
				source.x = 0*64;
				source.y = 3*64;
				source.w = 5*64;
				source.h = 1*64;
				name = "medium0Future";
				qtde = 5;
				break;
			case MEDIUM_1_FUTURE:
				source.x = 0*64;
				source.y = 4*64;
				source.w = 5*64;
				source.h = 1*64;
				name = "medium1Future";
				qtde = 5;
				break;
			case MEDIUM_2_FUTURE:
				source.x = 0*64;
				source.y = 5*64;
				source.w = 5*64;
				source.h = 1*64;
				name = "medium2Future";
				qtde = 5;
				break;
			case LARGE_0_FUTURE:
				source.x = 0*64;
				source.y = 0*64;
				source.w = 8*64;
				source.h = 1*64;
				name = "large0Future";
				qtde = 8;
				break;
			case LARGE_1_FUTURE:
				source.x = 0*64;
				source.y = 1*64;
				source.w = 8*64;
				source.h = 1*64;
				name = "large1Future";
				qtde = 8;
				break;
			default:
				Gdx.app.log("Platform","Erro!, plataforma tipo "+this.type+" inválida");
				throw new RuntimeException("Plataforma inválida");
		}
		
		// posição do rect de colisão
		if (type >= LITTLE_GREEN && type <= LARGE_GREEN)
			collY = 15;
		else if (type == LARGE_ICE || type == MEDIUM_0_ICE || type == MEDIUM_1_ICE)
			collY = 64;
		else if (type == MEDIUM_2_ICE)
			collY = 80;
		else if (type >= MEDIUM_0_FUTURE && type <= LARGE_1_FUTURE)
			collY = 20;
		
		this.camera = camera;
		
		for (int i = 0; i < qtde; i++)
			fishes.add(new Fish(camera, fishType, x + i * 64, y - 64 * 2));
		
		platformRegion = new TextureRegion(texture, source.x, source.y, source.w, source.h);
		platformRegion.flip(false,true);
		colision = new TextureRegion(new Texture(Gdx.files.internal("pixel.png")));
		
		initPos = new Vector2(x,y);
		dest.w = source.w;
		
		collRect.w = source.w;
		if (type >= LITTLE_GREEN && type <= LARGE_GREEN)
		{
			collRect.h = (int)(source.h*0.50f);
			dest.h = (int)(source.h * 0.8);
		}
		else
		{
			collRect.h = (int)(source.h);
			dest.h = (int)(source.h);
		}
		setPosition(initPos);
	}
	
	public void dispose (  ) {
		for (Fish fish: fishes)
			fish.dispose();
	}
	
	public static int getTypeByName ( String type ){
		if (type.equals("littleGreen"))
			return Platform.LITTLE_GREEN;
		else if (type.equals("mediumGreen"))
			return Platform.MEDIUM_GREEN;
		else if (type.equals("largeGreen"))
			return Platform.LARGE_GREEN;
		else if (type.equals("medium0Ice"))
			return Platform.MEDIUM_0_ICE;
		else if (type.equals("medium1Ice"))
			return Platform.MEDIUM_1_ICE;
		else if (type.equals("medium2Ice"))
			return Platform.MEDIUM_2_ICE;
		else if (type.equals("largeIce"))
			return Platform.LARGE_ICE;
		else if (type.equals("medium0Future"))
			return Platform.MEDIUM_0_FUTURE;
		else if (type.equals("medium1Future"))
			return Platform.MEDIUM_1_FUTURE;
		else if (type.equals("medium2Future"))
			return Platform.MEDIUM_2_FUTURE;
		else if (type.equals("large0Future"))
			return Platform.LARGE_0_FUTURE;
		else if (type.equals("large1Future"))
			return Platform.LARGE_1_FUTURE;
		
		Gdx.app.log("Platform","Plataforma.type \""+type+"\" não identificado");
		return -1;
	}
	
	public int getType() {
		return type;
	}
	
	public void reset (  ) {
		touchs = 0;
		int randomFish = (int)(Fish.MAX_FISH * Math.random());
		
		int i = 0;
		for (Fish fish: fishes)
		{
			fish.reset(new Vector2(collRect.x + i * 64.0f, collRect.y - 64.0f * 2.0f));
			fish.setType(randomFish);
			i++;
		}
		
		
		setState(ON_GAME);
		
		
		shakes = 3;
		shakeCount = 0;
		shake = false;
	}
	
	public void reset ( int fishType ) {
		touchs = 0;
		//int randomFish = (int)(Fish.MAX_FISH * Math.random());
		
		int i = 0;
		for (Fish fish: fishes)
		{
			fish.reset(new Vector2(collRect.x + i * 64.0f, collRect.y - 64.0f * 2.0f));
			fish.setType(fishType);
			i++;
		}
		
		
		setState(ON_GAME);
		
		
		shakes = 3;
		shakeCount = 0;
		shake = false;
	}
	
	public void setPosition ( Vector2 p ){
		super.setPosition(p);
		
		dest.x = (int)p.x;
		dest.y = (int)(p.y + collY);
		collRect.x = (int)p.x;
		collRect.y = (int)(p.y + collY);
		collRect.w = dest.w;
		int i = 0;
		for (Fish fish: fishes)
		{
			fish.setPosition(new Vector2(collRect.x + i * 64, collRect.y - 64 * 2));
			i++;
		}
	}
	
	public Vector2 getInitPosition (  ) {
		return initPos;
	}
	
	public Rect getCollRect (  )
	{
		return collRect;
	}
	
	public Rect getDest (  )
	{
		return dest;
	}
	
	public Vector<Fish> getFishes() {
		return fishes;
	}
	
	
	// conta as vezes tocado pelo jogador
	public void touched (  ) {
		if (type >= MEDIUM_0_FUTURE && type <= LARGE_1_FUTURE)
		{
			shake = true;
			shakeCount = 0;
			touchs++;
		}
	}
	
	public void draw ( SpriteBatch batch, OrthographicCamera camera ){
		float shakeY = 0;
		float opt[] = {-10,-8,-6,-3,-2,-1,1,2,3,6,8,10};
		if (shake)
		{
			shakeY = opt[(int)(Math.random()*opt.length)];
			shakeCount++;
			if (shakeCount > shakes)
			{
				shakeCount = 0;
				shake = false;
			}
		}
		batch.draw(platformRegion, pos.x, pos.y + shakeY, dest.w, dest.h);
	}
	
	
	public int update (  ) {
		for (Fish fish: fishes)
			fish.update();
		
		switch (getState())
		{
			case ON_GAME:
				if (type >= MEDIUM_0_FUTURE && type <= MEDIUM_2_FUTURE)
				{
					if (touchs > 1)//touchs começa em 0
					{
						setState(FALLING);
					}
				}
				
				if (type == LARGE_0_FUTURE || type == LARGE_1_FUTURE)
				{
					if (touchs > 3)//touchs começa em 0
					{
						setState(FALLING);
					}
				}
				break;
			
			case FALLING:
				pos.y += 5;
				setPosition(pos);
				break;
		}
		
		return getState();
	}
}
