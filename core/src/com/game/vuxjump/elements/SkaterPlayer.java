package com.game.vuxjump.elements;

import java.util.HashMap;
import java.util.Vector;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;
import com.game.vuxjump.elements.Player.PlayerConfig;

public class SkaterPlayer extends StateMachine {
	public final static int INACTIVE_STATE = -1;
	public final static int STOP_STATE = 0;
	public final static int RUN_STATE = 1;
	public final static int LOSE_STATE = 2;
	
	public boolean up = false, down = false;
	private Rect collRect, dest;
	
	private Rect dim;
	private Vector2 vel;
	private HashMap<String, TextureRegion> anim;
	private TextureRegion currAnim;
	public PlayerConfig config;
	private Vector<RoadObstacle> obstacles;
	private OrthographicCamera camera;
	private int[] score = new int[]{0,0,0,0,0,0};

	private static Vector<Texture> textures;
	
	
	
	
	public SkaterPlayer ( OrthographicCamera camera ) {
		
		
		this.camera = camera;
	}
	
	public class SkaterPlayerConfig {
		public Vector2 startPos; // posição inicial na fase
		public Vector2 acc; // aceleração
		public Vector2 velMax; // velocidades máximas
		public int startState; // estado inicial
		
		// precisa ser ajustando testando no programa
		public SkaterPlayerConfig (  ) {
			startPos = new Vector2(100,100);
			acc = new Vector2(0.5f, 0.1f);
			velMax = new Vector2(7,14);
			startState = INACTIVE_STATE;
		}
	}
	
	private static boolean destroy = true;
	public void dispose (  ) {
		if (destroy)
		{
			destroy = false;
			for (Texture tex: textures)
				tex.dispose();
		}
	}
	
	public int getScore ( int type ) {
		return score[type];
	}
	
	public void eatFishes (  ) {
		for (RoadObstacle p: obstacles)
			for (Fish fish: p.getFishes())
				if (fish.getState() == Fish.STOP_STATE && this.dim.boundingbox(fish.getCollRect()))
				{
					fish.gotoHUD();
												
					this.score[fish.getType()] += fish.getValue();
				}
	}
	
	
	public void addObstacles ( Vector<RoadObstacle> p ) {
		Vector<RoadObstacle> aux = new Vector<RoadObstacle>();
		for (RoadObstacle ob: p)
		{
			boolean has = false;		
			for (RoadObstacle ob2: obstacles)
				if (ob == ob2)
				{
					has = true;
					break;
				}
			
			if (has == false)
				aux.add(ob);
		}
		
		if (aux.size() > 0)
		{
			obstacles.addAll(aux);
			aux.clear();
		}
	}
	
	public void clearObstacles (  ) {
		obstacles.clear();
	}
	
	public void remObstacles ( Vector<RoadObstacle> p ) {
		Vector<RoadObstacle> aux = new Vector<RoadObstacle>();
		for (RoadObstacle ob: obstacles)
		{
			boolean has = false;		
			for (RoadObstacle ob2: p)
				if (ob == ob2)
				{
					has = true;
					break;
				}
			
			if (has == false)
				aux.add(ob);
		}
		
		if (aux.size() != 0)
		{
			obstacles.clear();
			obstacles = aux;
		}
	}
}
