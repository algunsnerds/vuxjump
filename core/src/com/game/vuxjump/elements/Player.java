package com.game.vuxjump.elements;

import java.util.HashMap;

import java.util.Map;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.game.vuxjump.GameState;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;

public class Player extends StateMachine {
	public final static int INACTIVE_STATE = -1;
	public final static int STAND_STATE = 0;
	public final static int WALK_STATE = 1;
	public final static int JUMP_STATE = 2;
	public static final int DIED_STATE = 3;
	
	public final static int LEFT_DIR = 0;
	public final static int RIGHT_DIR = 1;
	
	
	public boolean jump = false, left = false, right = false;
	public int dir = RIGHT_DIR;
	private Rect collRect;
	
	private Rect dim, boundary;
	private float counterRate = 0;
	private Vector2 vel, counterVel;
	public PlayerConfig config;
	private Platform currPlatform;
	private Vector<Platform> platforms, noCollPlatform;
	private int[] score = new int[]{0,0,0,0,0,0};
	private OrthographicCamera camera;
	private static String name = "null";
	private static boolean destroy = true;
	private static Vector<Texture> textures = null;
	private static HashMap<String, TextureRegion> anim = null;
	private TextureRegion currAnim;
	private int lifes;
	private static int totalLifes;
	
	public Player ( TextureAtlas atlas, OrthographicCamera camera ) {
		changeToChar("vux");//muda a imagem do player
		
		config = new PlayerConfig();
		this.camera = camera;
		dim = new Rect(0,0,80,80);
		boundary = new Rect(0,0,1024,600);
		vel = new Vector2();
		vel.y = config.velStartJump;
		counterVel = new Vector2();
		noCollPlatform = new Vector<Platform>();
		this.platforms = new Vector<Platform>();
		
		
		lifes = totalLifes;
		collRect = new Rect();;
		collRect.x = 0;
		collRect.y = 0;
		collRect.w = 57;
		collRect.h = (int)(80*0.30f);
		
		// nao funciona as setas não pega os eventos pera
		setPosition(new Vector2(0, 31));
		dir = RIGHT_DIR; 
		currAnim = anim.get("rightStand");
		setState(JUMP_STATE);
	}
	
	public void dispose (  ) {
		if (destroy)
		{
			destroy = false;
			for (Texture tex: textures)
				tex.dispose();
		}
	}
	
	public String getName() {
		return name;
	}
	
	public static void changeToChar ( String name ) {
		if (Player.name.equals("null"))
		{
			Player.name = name;
		}
		else if (Player.name.equals(name))
		{
			// evita recarregar a mesma textura várias vezes
			return;
		}
		
		if (textures == null)
		{
			textures = new Vector<Texture>();
			destroy = true;
		}
		else
		{
			for (Texture tex: textures)
				tex.dispose();
			
			textures.clear();
		}
		
		if (anim == null)
		{
			anim = new HashMap<String,TextureRegion>();
		}
		else
		{
			anim.clear();
		}
		
		Texture texAux;
		TextureRegion animAux;
	
		texAux = new Texture(Gdx.files.internal("player/"+name+"/stand.png"));
		textures.add(texAux);

		texAux = new Texture(Gdx.files.internal("player/"+name+"/stand.png"));
		textures.add(texAux);
		
		texAux = new Texture(Gdx.files.internal("player/"+name+"/moveJump1.png"));
		textures.add(texAux);
		
		texAux = new Texture(Gdx.files.internal("player/"+name+"/moveJump2.png"));
		textures.add(texAux);
		
		texAux = new Texture(Gdx.files.internal("player/"+name+"/standJumpUp.png"));
		textures.add(texAux);
		
		texAux = new Texture(Gdx.files.internal("player/"+name+"/standJumpDown.png"));
		textures.add(texAux);
		
		texAux = new Texture(Gdx.files.internal("player/"+name+"/standJumpUp.png"));
		textures.add(texAux);
		
		texAux = new Texture(Gdx.files.internal("player/"+name+"/standJumpDown.png"));
		textures.add(texAux);
		
		// carrega as regiões
		animAux = new TextureRegion(textures.get(0));
		animAux.flip(false, true);
		anim.put("rightStand", animAux);
		
		animAux = new TextureRegion(textures.get(1));
		animAux.flip(true, true);
		anim.put("leftStand", animAux);
		
		animAux = new TextureRegion(textures.get(2));
		animAux.flip(false, true);
		anim.put("rightWalkJump", animAux);
		
		animAux = new TextureRegion(textures.get(3));
		animAux.flip(true, true);
		anim.put("leftWalkJump", animAux);
		
		animAux = new TextureRegion(textures.get(4));
		animAux.flip(false, true);
		anim.put("rightStandJumpUp", animAux);
		
		animAux = new TextureRegion(textures.get(5));
		animAux.flip(false, true);
		anim.put("rightStandJumpDown", animAux);
		
		animAux = new TextureRegion(textures.get(6));
		animAux.flip(true, true);
		anim.put("leftStandJumpUp", animAux);
		
		animAux = new TextureRegion(textures.get(7));
		animAux.flip(true, true);
		anim.put("leftStandJumpDown", animAux);
	}
	
	public int getLives (  ) {
		return lifes;
	}
	
	public void setTotalLifes ( int tl ) {
		totalLifes = tl;
	}
	
	public class PlayerConfig {
		public int startDir; // direção inicial
		public Vector2 startPos; // posição inicial na fase
		public Vector2 acc; // aceleração
		public Vector2 velMax; // velocidades máximas
		public Vector2 velMin; // velocidade minima
		public float gravity; // gravidade
		public float velMaxJump; // velocidade máxima do pulo ao descer
		public float velStartJump; // velocidade inicial ao pular
		public int startState; // estado inicial
		public float stopRate; // taxa de diminuição para ser parado
		
		// precisa ser ajustando testando no programa
		public PlayerConfig (  ) {
			startDir = RIGHT_DIR;
			startPos = new Vector2(100,100);
			acc = new Vector2(0.5f, 0.1f);
			velMax = new Vector2(7,14);
			velMin = new Vector2(0.005f,0.005f);
			gravity = 0.8f;
			velMaxJump = 17;
			velStartJump = -18;
			stopRate = 0.97f;
			startState = INACTIVE_STATE;
		}
	}
	
	@Override
	public void setPosition ( Vector2 p ){
		collRect.x = (int)p.x + 15;
		collRect.y = (int)p.y + (80 - collRect.h);
		
		dim.x = (int)p.x;
		dim.y = (int)p.y;
		pos.x = p.x;
		pos.y = p.y;
		super.setPosition(pos.x, pos.y);
	}
	
	public void reset ( Platform plat ){
		Vector2 startPos = new Vector2(plat.getPosition().x + plat.getCollRect().w/2 - collRect.w/2, plat.getCollRect().y - dim.h);
		setPosition(startPos);
		vel.x = vel.y = 0;
		for (int i = 0; i < score.length; i++)
			score[i] = 0;
		
		lifes = totalLifes;
		
		config.startPos.x = pos.x;
		config.startPos.y = pos.y;
		
		Vector<Platform> vet = new Vector<Platform>();
		vet.add(plat);
		addPlatforms(vet);
		collisionVer();
	}
	
	public int getScore ( int type ) {
		return score[type];
	}
	
	public void addPlatforms ( Vector<Platform> p ) {
		Vector<Platform> aux = new Vector<Platform>();
		for (Platform plat: p)
		{
			boolean has = false;		
			for (Platform plat2: platforms)
				if (plat == plat2)
				{
					has = true;
					break;
				}
			
			if (has == false)
				aux.add(plat);
		}
		
		if (aux.size() > 0)
		{
			platforms.addAll(aux);
			aux.clear();
		}
	}
	
	public void clearPlatforms (  ) {
		platforms.clear();
	}
	
	public void remPlatforms ( Vector<Platform> p ) {
		Vector<Platform> aux = new Vector<Platform>();
		for (Platform plat: platforms)
		{
			boolean has = false;		
			for (Platform plat2: p)
				if (plat == plat2)
				{
					has = true;
					break;
				}
			
			if (has == false)
				aux.add(plat);
		}
		
		if (aux.size() != 0)
		{
			platforms.clear();
			platforms = aux;
		}
	}
	
	final Vector <Platform> plat1 = new Vector<Platform>();
	final Vector <Platform> plat2 = new Vector<Platform>();
	
	public void updateCollision (  ){
		setPosition(pos);
		
		plat1.clear();
		plat2.clear();
		plat1.addAll(touchPlatform());
		
		if (vel.y < 0 || vel.x != 0)
		{
			for (Platform p: plat1)
			{
				for (Platform q: noCollPlatform)
					if (p == q)
						plat2.add(q);
			}
			
			
			if (plat2.size() == 0)
			{
				noCollPlatform.clear();
				if (plat1.size() > 0)
					noCollPlatform.addAll(plat1);
			}
			else
			{
				noCollPlatform.clear();
				noCollPlatform.addAll(plat2);
			}
				
		}
		
		if (vel.y > 0)
		{
			if (plat1.size() > 0)
			{
				noCollPlatform.clear();
				noCollPlatform.addAll(plat1);
			}
			else
			{
				noCollPlatform.clear();
			}
		}
		
	}
	
	public Vector<Platform> touchPlatform (  ){
		final Vector<Platform> p = new Vector<Platform>();
		p.clear();
		for (Platform plat: platforms)
		{	
			if (plat != currPlatform && plat.getCollRect().boundingbox(collRect) == true)
			{
				p.add(plat);
			}
		}
		
		return p;
	}
	
	public Platform getCurrPlatform (  ) {
		return currPlatform;
	}
	
	public void applyForce ( float x, float y, float r ) {
		counterVel.set(x,y);
		counterRate = r;
	}
	
	public Rect getCollRect (  ){
		return collRect;
	}
	
	public int getDir (  ) {
		return dir;
	}
	
	public boolean collisionPlatform ( Platform p ){
		for (Platform q: noCollPlatform)
			if (q == p)
			{
				return false;
			}
		
		return true;
	}
	
	public boolean collisionVer(  ){
		setPosition(pos);
		for (Platform plat: platforms)
		{
			if (vel.y > 0 && collisionPlatform(plat) && plat.getCollRect().boundingbox(collRect) == true)
			{
				currPlatform = plat;
				Rect collR = plat.getCollRect();
				Vector2 p = new Vector2(pos.x,pos.y);
				p.y = collR.y - 80;
				setPosition(p);
				return true;
			}
		}
		
		return false;
	}
	
	public boolean collisionHor (  ) {
		if (GameState.getOrientationValue().equals("landscape"))
		{
			if (getPosition().x < boundary.x)
			{
				setPosition(new Vector2(boundary.x, getPosition().y));
				vel.x = 0;
				return true;
			}
			else if (getPosition().x + collRect.w > boundary.x + boundary.w)
			{
				Vector2 p;
				p = new Vector2(boundary.x + boundary.w - collRect.w, getPosition().y);
				
				setPosition(p);
				vel.x = 0;
				return true;
			}
		}
		else
		{
			Vector2 p = new Vector2(getPosition());
			if (collRect.x + collRect.w/2 < boundary.x)
			{
				p.x = boundary.x + (boundary.w - collRect.w);
				setPosition(p);
				return true;
			}
			else if (collRect.x + collRect.w/2 > boundary.x + boundary.w)
			{
				p.x = boundary.x;
				setPosition(p);
				return true;
			}
		}
		
		return false;
	}
	
	public boolean isGround (  ){
		setPosition(pos);
		if (currPlatform != null)
		{
			Rect collAux = new Rect(collRect.x,collRect.y, collRect.w, collRect.h + (int)config.velMax.y/2);
			if (currPlatform.getCollRect().boundingbox(collAux) == true)
			{
				currPlatform.touched();
				return true;
			}
		}
		
		return false;
	}
	
	
	public boolean keyboardInput ( int key, boolean down ) {
		if (key == Keys.RIGHT)
		{
			if (down)
			{
				right = true;
				left = false;
			}
			else
			{
				right = false;
			}
		}
		
		if (key == Keys.LEFT)
		{
			if (down)
			{
				left = true;
				right = false;
			}
			else
				left = false;
		}
		
		if (key == Keys.UP || key == Keys.BACK)
		{
			if (GameState.getOrientationValue().equals("landscape"))
				setPosition(new Vector2(getPosition().x, camera.position.y - camera.viewportHeight/2));
			else
				setPosition(new Vector2(getPosition().x, camera.position.y - camera.viewportHeight/2));
			return true;
		}
		
		jump = true;
		return false;
	}
	
	public boolean touchInput ( int screenX, int screenY, int pointer, int button, boolean down ) {
		/*
		 * screenX e Y são relativos ao screen e tem que usar camera.unproject()
		 * para pegar as coordenadas relativas ao mundo (1024x600)
		 */

		if (down)
		{
			if (screenX > Gdx.graphics.getWidth()/2)
			{
				right = true;
				left = false;
			}
			else
			{
				right = false;
				left = true;
			}
		}
		else
		{
			if (screenX > Gdx.graphics.getWidth()/2)
			{
				if (right)
					right = false;
			}
			else
				if (left)
					left = false;
		}
		
		return false;
	}
	
	public void setBoundary ( OrthographicCamera camera ) {
		boundary.x = (int)(camera.position.x - camera.viewportWidth/2);
		boundary.y = (int)(camera.position.y - camera.viewportHeight/2);
		boundary.w = (int)camera.viewportWidth;
		boundary.h = (int)camera.viewportHeight;
	}
	
	public Rect getBoundary (  ) {
		return boundary;
	}
	
	public Rect getDim (  ) {
		return dim;
	}
	
	public void eatFishes (  ) {
		for (Platform p: platforms)
			for (Fish fish: p.getFishes())
				if (fish.getState() == Fish.STOP_STATE && this.dim.boundingbox(fish.getCollRect()))
				{
					fish.gotoHUD();
								
					this.score[fish.getType()] += fish.getValue();
				}
	}
	
	@Override
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		if (currAnim != null)
		{
			batch.draw(currAnim, pos.x, pos.y, dim.w, dim.h);
			//batch.draw(colision, collRect.x, collRect.y, collRect.w, collRect.h);
		}
	}
	
	@Override
	public int update (  ) {
		switch (getState())
		{
			case STAND_STATE:
				if (isGround() == false)
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStandDown");
					else
						currAnim = anim.get("rightStandDown");
					
					vel.setZero();
					setState(JUMP_STATE);
					break;
				}

				if (jump)
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStandJumpUp");
					else
						currAnim = anim.get("rightStandJumpUp");
					
					
					vel.y = config.velStartJump;
					pos.y +=  vel.y;
					setState(JUMP_STATE);
					break;
				}
				/*
				if (left)
				{
					dir = LEFT_DIR;
					currAnim = anim.get("leftWalk");
					
					vel.zero();
					setState(WALK_STATE);
					break;
				}
				else if (right)
				{
					dir = RIGHT_DIR;
					currAnim = anim.get("rightWalk");
					
					vel.zero();
					setState(WALK_STATE);
					break;
				}
				*/
				
				
				collisionHor();
				// para pular todo tempo
				jump = true;
				break;
			
			case JUMP_STATE:
				
				
				// para pegar os peixes no ar
				eatFishes();
				
				if (isGround())
				{
					
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStand");
					else
						currAnim = anim.get("rightStand");
					
					vel.y = config.velStartJump;
					//pos.y += vel.y;
					collisionVer();
					//setState(STAND_STATE);
					jump = true;
				}
				
				vel.x += counterVel.x;
				counterVel.x = counterVel.x * counterRate;
				if (counterVel.x < 0.5 && counterVel.x >= -0.5)
				{
					counterRate = 0;
					counterVel.x = 0;
				}
				
				
				
				vel.y += config.gravity;
				if (vel.y > config.velMaxJump)
					vel.y = config.velMaxJump;
				
				pos.y += vel.y;
				collisionVer();
				setPosition(pos);
				
				if (vel.y < 0)
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStandJumpUp");
					else
						currAnim = anim.get("rightStandJumpUp");
				}
				
		///////////////// Eixo X
				if (left)
				{
					vel.x += -config.acc.x;
					dir = LEFT_DIR;
					currAnim = anim.get("leftWalkJump");
				}
				else if (right)
				{
					vel.x += config.acc.x;
					dir = RIGHT_DIR;
					currAnim = anim.get("rightWalkJump");
				}
				else
				{
					vel.x = vel.x * config.stopRate;
				}
				
				if (vel.x >= -config.velMin.x && vel.x <= config.velMin.x)
					vel.x = 0;
				
				if (vel.x > config.velMax.x)
					vel.x = config.velMax.x;
				else if (vel.x < -config.velMax.x)
					vel.x = -config.velMax.x;
				
				pos.x += vel.x;
				collisionHor();
				
				if (vel.y > 0)
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStandJumpDown");
					else
						currAnim = anim.get("rightStandJumpDown");
				}
				break;
			
			case DIED_STATE:
				break;
				
			default:
				break;
		}
		
		// altamente necessario
		updateCollision();
		
		setBoundary(camera);
		
		return getState();
	}

	

	
}
