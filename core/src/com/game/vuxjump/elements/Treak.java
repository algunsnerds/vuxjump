package com.game.vuxjump.elements;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;

public class Treak extends StateMachine {// cara essa classe é minh DEIxa eu ver ela {
	public final static int INACTIVE_STATE = -1;
	public final static int STAND_STATE = 0;
	public final static int WALK_STATE = 1;
	public final static int JUMP_STATE = 2;
	
	public final static int LEFT_DIR = 0;
	public final static int RIGHT_DIR = 1;
	
	public boolean jump = false, left = false, right = false;
	public int dir = RIGHT_DIR;
	private Rect collRect, collPlayer;
	
	private Rect dim;
	private float counterRate = 0;
	private Vector2 vel, counterVel;
	private Map<String, TextureRegion> anim;
	private TextureRegion currAnim;
	public PlayerConfig config;
	private Platform currPlatform;
	private Vector<Platform> platforms, noCollPlatform;
	
	private Player player;

	private static Vector<Texture> textures = null;
	
	public Treak ( TextureAtlas atlas, Player player ) {
		anim = new HashMap<String,TextureRegion>();
		
		this.player = player;
		
		TextureRegion animAux;
		Texture texAux;
		
		if (textures == null)
		{
			textures = new Vector<Texture>();
			texAux = new Texture(Gdx.files.internal("enemy/stand.png"));
			textures.add(texAux);
			
			texAux = new Texture(Gdx.files.internal("enemy/stand.png"));
			textures.add(texAux);
			
			texAux = new Texture(Gdx.files.internal("enemy/moveJump1.png"));
			textures.add(texAux);
			
			texAux = new Texture(Gdx.files.internal("enemy/moveJump2.png"));
			textures.add(texAux);
			
			texAux = new Texture(Gdx.files.internal("enemy/standJumpUp.png"));
			textures.add(texAux);
			
			texAux = new Texture(Gdx.files.internal("enemy/standJumpDown.png"));
			textures.add(texAux);

			texAux = new Texture(Gdx.files.internal("enemy/standJumpUp.png"));
			textures.add(texAux);
			
			texAux = new Texture(Gdx.files.internal("enemy/standJumpDown.png"));
			textures.add(texAux);
		}
		
		animAux = new TextureRegion(textures.get(0));
		animAux.flip(false, true);
		anim.put("rightStand", animAux);
		
		animAux = new TextureRegion(textures.get(1));
		animAux.flip(true, true);
		anim.put("leftStand", animAux);
		
		animAux = new TextureRegion(textures.get(2));
		animAux.flip(false, true);
		anim.put("rightWalkJump", animAux);
		
		animAux = new TextureRegion(textures.get(3));
		animAux.flip(true, true);
		anim.put("leftWalkJump", animAux);
		
		animAux = new TextureRegion(textures.get(4));
		animAux.flip(false, true);
		anim.put("rightStandJumpUp", animAux);
		
		animAux = new TextureRegion(textures.get(5));
		animAux.flip(false, true);
		anim.put("rightStandJumpDown", animAux);
		
		animAux = new TextureRegion(textures.get(6));
		animAux.flip(true, true);
		anim.put("leftStandJumpUp", animAux);
		
		animAux = new TextureRegion(textures.get(7));
		animAux.flip(true, true);
		anim.put("leftStandJumpDown", animAux);
		
		dim = new Rect(0,0,128,128);
		vel = new Vector2();
		counterVel = new Vector2();
		noCollPlatform = new Vector<Platform>();
		this.platforms = new Vector<Platform>();
		config = new PlayerConfig();
		
		collRect = new Rect();
		collRect.x = 0;
		collRect.y = 0;
		collRect.w = 30;
		collRect.h = (int)(dim.h*0.25f);
		collPlayer = new Rect(0,0,collRect.w,collRect.h);
		
		setPosition(new Vector2(300, 10));
		dir = LEFT_DIR; 
		currAnim = anim.get("leftStand");
		setState(JUMP_STATE);
	}
	
	static boolean destroy = true;
	public void dispose (  ) {
		if (destroy == true)
		{
			destroy = false;
			for (Texture tex: textures)
				tex.dispose();
		}
	}
	
	
	public class PlayerConfig {
		public int startDir; // direção inicial
		public Vector2 startPos; // posição inicial na fase
		public Vector2 acc; // aceleração
		public Vector2 velMax; // velocidades máximas
		public Vector2 counterVel;
		public float counterRate;
		public float gravity; // gravidade
		public float velMaxJump; // velocidade máxima do pulo ao descer
		public float velStartJump; // velocidade inicial ao pular
		public int startState; // estado inicial
		public float changeDir;// taxa de troca direção quando em repouso
		public float timeOnStand;//tempo esperando até trocar de direção
		// precisa ser ajustando testando no programa
		public PlayerConfig (  ){
			startDir = RIGHT_DIR;
			startPos = new Vector2(100,100);
			acc = new Vector2(0.2f, 0.1f);
			velMax = new Vector2(7,14);
			counterVel = new Vector2(2.5f,5);
			counterRate = 0.9f;
			gravity = 0.8f;
			velMaxJump = 17;
			velStartJump = -9;
			changeDir = 0.3f;
			timeOnStand = 1.4f;//segundos
			startState = INACTIVE_STATE;
		}
	}
	
	@Override
	public void setPosition ( Vector2 p ){
		collRect.x = (int)p.x + (dim.w - collRect.w)/2;
		collRect.y = (int)p.y + (dim.h - collRect.h);
		collPlayer.x = (int)p.x + (dim.w - collPlayer.w)/2;
		collPlayer.y = (int)p.y + (dim.h - collPlayer.h);
		
		pos.x = p.x;
		pos.y = p.y;
		
		dim.x = collRect.x;
		dim.y = collRect.y;
		
		super.setPosition(pos.x, pos.y);
	}
	
	public void setPosition ( Platform p ) {
		Vector2 aux = new Vector2();
		Rect cr = p.getCollRect();
		aux.x = cr.x + (cr.w - dim.w)/2;
		aux.y = cr.y - dim.h/2;
		setPosition(aux);
		config.startPos.x = pos.x;
		config.startPos.y = pos.y;
		
		dim.x = collRect.x;
		dim.y = collRect.y;
		
		Vector<Platform> vet = new Vector<Platform>();
		vet.add(p);
		addPlatforms(vet);
		collisionVer();
	}
	
	public void reset ( Platform plat ){
		setPosition(config.startPos.x, config.startPos.y);
		vel.x = vel.y = 0;
		
		Vector2 startPos = new Vector2(plat.getPosition().x + plat.getCollRect().w/2 - collRect.w/2, plat.getCollRect().y - dim.h);
		setPosition(startPos);
		vel.x = vel.y = 0;
		
		config.startPos.x = pos.x;
		config.startPos.y = pos.y;
		
		Vector<Platform> vet = new Vector<Platform>();
		vet.add(plat);
		addPlatforms(vet);
		collisionVer();
		
		if (Math.random() >= 0.5)
			dir = RIGHT_DIR;
		else
			dir = LEFT_DIR;
	}
	
	final Vector <Platform> plat1 = new Vector<Platform>();
	final Vector <Platform> plat2 = new Vector<Platform>();
	
	public void updateCollision (  ){
		setPosition(pos);
		
		plat1.clear();
		plat2.clear();
		plat1.addAll(touchPlatform());
		
		if (vel.y < 0 || vel.x != 0)
		{
			for (Platform p: plat1)
			{
				for (Platform q: noCollPlatform)
					if (p == q)
						plat2.add(q);
			}
			
			
			if (plat2.size() == 0)
			{
				noCollPlatform.clear();
				if (plat1.size() > 0)
					noCollPlatform.addAll(plat1);
			}
			else
			{
				noCollPlatform.clear();
				noCollPlatform.addAll(plat2);
			}
				
		}
		
		if (vel.y > 0)
		{
			if (plat1.size() > 0)
			{
				noCollPlatform.clear();
				noCollPlatform.addAll(plat1);
			}
			else
			{
				noCollPlatform.clear();
			}
		}
		
	}
	
	public void addPlatforms ( Vector<Platform> p ) {
		Vector<Platform> aux = new Vector<Platform>();
		for (Platform plat: p)
		{
			boolean has = false;		
			for (Platform plat2: platforms)
				if (plat == plat2)
				{
					has = true;
					break;
				}
			
			if (has == false)
				aux.add(plat);
		}
		
		if (aux.size() > 0)
		{
			platforms.addAll(aux);
			aux.clear();
		}
	}
	
	public void clearPlatforms (  ) {
		platforms.clear();
	}
	
	public void remPlatforms ( Vector<Platform> p ) {
		Vector<Platform> aux = new Vector<Platform>();
		for (Platform plat: platforms)
		{
			boolean has = false;		
			for (Platform plat2: p)
				if (plat == plat2)
				{
					has = true;
					break;
				}
			
			if (has == false)
				aux.add(plat);
		}
		
		if (aux.size() != 0)
		{
			platforms.clear();
			platforms = aux;
		}
	}
	
	public Vector<Platform> touchPlatform (  ){
		final Vector<Platform> p = new Vector<Platform>();
		p.clear();
		for (Platform plat: platforms)
		{
			if (plat != currPlatform && plat.getCollRect().boundingbox(collRect) == true)
			{
				p.add(plat);
			}
		}
		
		return p;
	}
	
	public Platform getCurrPlatform (  ) {
		return currPlatform;
	}
	
	public void applyForce ( float x, float y, float r ) {
		counterVel.set(x,y);
		counterRate = r;
	}
	
	public Rect getCollRect (  ){
		return collPlayer;
	}
	
	public boolean collisionPlatform ( Platform p ){
		for (Platform q: noCollPlatform)
			if (q == p)
			{
				return false;
			}
		
		return true;
	}
	
	public boolean collisionVer(  ){
		setPosition(pos);
		for (Platform plat: platforms)
		{
			if (vel.y > 0 && collisionPlatform(plat) && plat.getCollRect().boundingbox(collRect) == true)
			{
				currPlatform = plat;
				Rect collR = plat.getCollRect();
				Vector2 p = new Vector2(pos.x,pos.y);
				p.y = collR.y - dim.h;
				setPosition(p);
				return true;
			}
		}
		
		return false;
	}
	
	public void collisionHor (  ){
		
	}
	
	public boolean isGround (  ){
		setPosition(pos);
		if (currPlatform != null)
		{
			Rect collAux = new Rect(collRect.x,collRect.y, collRect.w, collRect.h + (int)config.velMax.y/2);
			if (currPlatform.getCollRect().boundingbox(collAux) == true)
			{
				return true;
			}
		}
		
		return false;
	}
	
	public boolean inPlayerView (  )
	{
		return dim.boundingbox(player.getBoundary());
	}
	
	public void keyboardInput ( int key, boolean down ) {

	}
	
	public void touchInput ( int screenX, int screenY, int pointer, int button, boolean down ) {

	}
	
	public Rect getDim (  ) {
		return dim;
	}
	
	
	Texture colision = new Texture(Gdx.files.internal("redPixel.png"));
	@Override
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		if (currAnim != null)
		{
			batch.draw(currAnim, pos.x, pos.y, dim.w, dim.h);
			batch.draw(colision, collPlayer.x, collPlayer.y, collPlayer.w, collPlayer.h);
		}
	}
	
	float countTime = 0;
	
	@Override
	public int update (  ) {
		switch (getState())
		{
			case STAND_STATE:
				countTime += Gdx.graphics.getDeltaTime();
				
				if (countTime > config.timeOnStand)
				{
					countTime = 0;
					
					//inverte as animações
					int d = LEFT_DIR;
					if (Math.random() < 0.5)
						d = LEFT_DIR;
					else
						d = RIGHT_DIR;
					
					if (d == LEFT_DIR)
						currAnim = anim.get("rightStand");
					else
						currAnim = anim.get("leftStand");
				}
				
				if (player.getCurrPlatform() == currPlatform)
				{
					jump = true;
					vel.setZero();
				}
				
				if (player.getPosition().x < getPosition().x)
				{
					left = true;
					right = false;
				}
				else if (player.getPosition().x >= getPosition().x)
				{
					left = false;
					right = true;
				}
				
				if (isGround() == false)
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStandDown");
					else
						currAnim = anim.get("rightStandDown");
					
					vel.setZero();
					setState(JUMP_STATE);
					break;
				}

				if (jump)
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStandJumpUp");
					else
						currAnim = anim.get("rightStandJumpUp");
					
					
					vel.y = config.velStartJump;
					pos.y +=  vel.y;
					setState(JUMP_STATE);
					break;
				}
				/*
				if (left)
				{
					dir = LEFT_DIR;
					currAnim = anim.get("leftWalk");
					
					vel.zero();
					setState(WALK_STATE);
					break;
				}
				else if (right)
				{
					dir = RIGHT_DIR;
					currAnim = anim.get("rightWalk");
					
					vel.zero();
					setState(WALK_STATE);
					break;
				}
				*/
				
				
				collisionHor();
				break;
			
			case JUMP_STATE:
				
				if (player.getCollRect().boundingbox(collPlayer))
				{
					if (dir == LEFT_DIR)
					{
						player.applyForce(-config.counterVel.x,config.counterVel.y, config.counterRate);
						applyForce(config.counterVel.x,config.counterVel.y, config.counterRate/4);
					}
					else
					{
						player.applyForce(config.counterVel.x,config.counterVel.y, config.counterRate);
						applyForce(-config.counterVel.x,config.counterVel.y, config.counterRate/4);
					}
				}
				
				if (left)
				{
					vel.x += -config.acc.x;
					dir = LEFT_DIR;
					currAnim = anim.get("leftWalkJump");
				}
				else if (right)
				{
					vel.x += config.acc.x;
					dir = RIGHT_DIR;
					currAnim = anim.get("rightWalkJump");
				}
				else
				{
					//vel.x = 0;
				}
				
				
				if (isGround())
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStand");
					else
						currAnim = anim.get("rightStand");
					
					collisionVer();
					jump = false;
					//vel.y = 0;
					setState(STAND_STATE);
					break;
				}
				
				vel.x += counterVel.x;
				counterVel.x = counterVel.x * counterRate;
				if (counterVel.x < 0.5 && counterVel.x >= -0.5)
				{
					counterRate = 0;
					counterVel.x = 0;
				}
				
				if (vel.x > config.velMax.x)
					vel.x = config.velMax.x;
				else if (vel.x < -config.velMax.x)
					vel.x = -config.velMax.x;
				
				pos.x += vel.x;
				collisionHor();
				
				vel.y += config.gravity;
				if (vel.y > config.velMaxJump)
					vel.y = config.velMaxJump;
				
				pos.y += vel.y;
				collisionVer();
				setPosition(pos);
				
				if (vel.y > 0)
				{
					if (dir == LEFT_DIR)
						currAnim = anim.get("leftStandJumpDown");
					else
						currAnim = anim.get("rightStandJumpDown");
				}
				break;
		}
		
		// altamente necessario
		updateCollision();
		
		return getState();
	}
}
