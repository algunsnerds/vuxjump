package com.game.vuxjump.elements;

import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.game.vuxjump.Rect;

/*
 * Bem parecido com o PlatformGroup
 */

public class RoadGroup {
	int id = -1;
	Vector<RoadObstacle> obstacle;
	OrthographicCamera camera;
	Rect dim; // dimensão do grupo
	Vector2 pos;
	boolean reseted = false;
	
	public RoadGroup  ( OrthographicCamera camera, int id, String path ) {
		this.id = id;
		this.camera = camera;
		dim = new Rect();
		pos = new Vector2();
	}
	
	public void dispose (  ) {
		for (RoadObstacle ob: obstacle)
			ob.dispose();
	}
	
	public int getID (  ) {
		return id;
	}
	
	public Vector<RoadObstacle> getObstacles (  ) {
		return obstacle;
	}
	
	public RoadObstacle getObstacle ( int i ) {
		return obstacle.get(i);
	}
	
	public void setPosition ( Vector2 p ) {
		pos.x = p.x;
		pos.y = p.y;
		dim.x = (int)p.x;
		dim.y = (int)p.y;
		
		for (RoadObstacle ob: obstacle)
		{
			p.x = ob.getInitPosition().x + pos.x;
			p.y = ob.getInitPosition().y + pos.y;
			ob.setPosition(p);
		}
	}
	
	public Vector<RoadObstacle> getObstacleOnCamera (  ) {
		Vector <RoadObstacle> vec = new Vector<RoadObstacle>();
		Rect camRect = new Rect();
		camRect.x = (int)(camera.position.x - camera.viewportWidth/2); 
		camRect.y = (int)(camera.position.y - camera.viewportHeight/2);
		camRect.w = (int)camera.viewportWidth;
		camRect.h = (int)camera.viewportHeight;
		
		
		/*
		 * Adiciona plataformas que colidem com a camera
		 */
		for (RoadObstacle ob: obstacle)
		{
			if (camRect.boundingbox(ob.getCollRect()))
			{
				vec.add(ob);
			}
		}
		
		/*
		 * Remove as plataformas que não estão com os seus centros na camera
		 */
		Vector2 point = new Vector2();
		for (RoadObstacle ob: obstacle)
		{
			point.x = ob.getCollRect().x + ob.getCollRect().w/2;
			point.y = ob.getCollRect().y;
			if (camRect.pointbox(point) == false)
			{
				vec.remove(ob);
			}
		}
		
		return vec;
	}
	
	public void loadObstacles ( Texture texture,  String path ) {
		// remove as plataformas anteriores
		dispose();
		
		JsonReader reader = new JsonReader();
		JsonValue value, child = reader.parse(Gdx.files.internal(path));
		
		value = child.child; 
		while(value != null){
			Object[] atts = {
					value.name,//type
					value.child.get("fish").asString(),
					value.child.get("x").asInt(),
					value.child.get("y").asInt(),
			};
			
			int type = -1;
			type = RoadObstacle.getTypeByName((String)atts[0]);
			
			int fishType = -1;
			fishType = Fish.getTypeByName((String)atts[1]);
			// não adicionar plataformas diretamente no platforms mas somente usando o método abaixo
			addObstacle(new RoadObstacle(camera,texture,type,fishType, (Integer)atts[2],(Integer)atts[3]));
			
			value = value.next;
		}
		
	}

	public void addObstacle ( RoadObstacle ro ) {
		obstacle.add(ro);
		
		if (obstacle.size() == 1)
			return;
		
		// atualiza a dimensão do grupo
		float x1 = obstacle.get(0).getPosition().x;
		float x2 = obstacle.get(0).getPosition().x + obstacle.get(0).getDest().w;
		
		float y1 = obstacle.get(0).getPosition().x;
		float y2 = obstacle.get(0).getPosition().x + obstacle.get(0).getDest().w;
		for (RoadObstacle ob: obstacle)
		{
			// eixo X
			if (ob.getPosition().x < x1)
				x1 = ob.getPosition().x;
			
			if (ob.getPosition().x + ob.getDest().w > x2)
				x2 = ob.getPosition().x + ob.getDest().w;
			
			// eixo Y
			if (ob.getPosition().y < y1)
				y1 = ob.getPosition().y;
			
			if (ob.getPosition().y + ob.getDest().h > y2)
				y2 = ob.getPosition().y + ob.getDest().h;
		}
		
		
		dim.w = (int)(x2 - x1);
		dim.h = (int)(y2 - y1);
		dim.x = (int)x1;
		dim.y = (int)y1; //devia ser uma posição fixa, como zero
	}
	
	
}
