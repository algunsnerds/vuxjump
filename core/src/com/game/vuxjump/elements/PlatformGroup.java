package com.game.vuxjump.elements;


import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;

public class PlatformGroup extends StateMachine {
	// types
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	
	protected int id = -1;
	protected int type;
	protected Vector<Platform> platforms;
	protected Rect dim; // dimensão do grupo
	protected Vector<Integer> connect; // id de grupos que se encaixam à direita deste e este à esquerda deles
	private boolean reseted;
	
	protected float grayFish = 3, greenFish = 3, redFish = 3;
	
	Texture pixel;
	TextureRegion pixelR;
	public PlatformGroup ( OrthographicCamera camera, int type, int id ) {
		this.id = id;
		this.type = type;
		dim = new Rect(0,0,(int)camera.viewportWidth, (int)camera.viewportHeight);
		platforms = new Vector<Platform>();
		connect = new Vector<Integer>();
		reseted = false;
		pixel = new Texture(Gdx.files.internal("redPixel.png"));
		pixelR = new TextureRegion(pixel);
	}
	/*
	 * Retorna um tipo de peixe para ser usado por uma plataforma.
	 * Aqui é um algoritmo da roleta para sortear os peixes
	 */
	public int getFishes (  ) {
		// na mesma ordem dos peixes em Fish
		final float values[] = {grayFish, greenFish, redFish};
		
		float total = greenFish + redFish + grayFish;
		float rand = (float)(total*Math.random());
		float totalPartial = 0;
		int i = 0;
		totalPartial = values[0];
		for (; totalPartial < rand; )
		{
			totalPartial += values[++i];
		}
		
		if (i >= values.length)
		{
			Gdx.app.log("PlaformGroup","Erro de ID do peixe, é muito grande, usando 0 como padrão");
			i = 0;
		}
		
		return i;
	}
	
	public int getID (  ) {
		return id;
	}
	
	public Vector<Platform> getPlatforms (  ) {
		return platforms;
	}
	
	public Rect getDim (  ) {
		return dim;
	}
	
	public void setDimH ( int h ) {
		dim.h = h;
	}
	
	public void unreset (  ) {
		reseted = false;
	}
	
	public boolean connectWith ( int g ) {
		for (Integer i: connect)
			if (i == g)
				return true;
		return false;
	}
	
	public Vector<Platform> getPlatformOnCamera ( OrthographicCamera camera ) {
		Vector <Platform> vec = new Vector<Platform>();
		Rect camRect = new Rect();
		camRect.x = (int)(camera.position.x - camera.viewportWidth/2); 
		camRect.y = (int)(camera.position.y - camera.viewportHeight/2);
		camRect.w = (int)camera.viewportWidth;
		camRect.h = (int)camera.viewportHeight;
		
		
		/*
		 * Adiciona plataformas que colidem com a camera
		 */
		for (Platform plat: platforms)
		{
			if (camRect.boundingbox(plat.getCollRect()))
			{
				vec.add(plat);
			}
		}
		
		/*
		 * Remove as plataformas que não estão com os seus centros na camera
		 */
		Vector2 point = new Vector2();
		for (Platform plat: vec)
		{
			point.x = plat.getCollRect().x + plat.getCollRect().w/2;
			point.y = plat.getCollRect().y;
			if (camRect.pointbox(point) == false)
			{
				vec.remove(plat);
			}
		}
		
		return vec;
	}
	
	public Platform getPlatform ( int i ) {
		for (int j = 0; j < platforms.size(); j++)
			if (j == i)
				return platforms.get(j);
		
		return null;
	}
	
	public void addPlatform ( Platform p ) {
		platforms.add(p);
		
		if (platforms.size() == 1)
			return;
		
		// atualiza a dimensão do grupo
		float x1 = platforms.get(0).getPosition().x;
		float x2 = platforms.get(0).getPosition().x + platforms.get(0).getDest().w;
		
		float y1 = platforms.get(0).getPosition().x;
		float y2 = platforms.get(0).getPosition().x + platforms.get(0).getDest().w;
		for (Platform plat: platforms)
		{
			if (type == PlatformGroup.HORIZONTAL)
			{
				// pega a menor a esquerda
				if (plat.getPosition().x < x1)
					x1 = plat.getPosition().x;
				
				if (plat.getPosition().x + plat.getDest().w > x2)
					x2 = plat.getPosition().x + plat.getDest().w;
			}
			
			if (type == PlatformGroup.VERTICAL)
			{
				if (plat.getPosition().y < y1)
					y1 = plat.getPosition().y;
				
				if (plat.getPosition().y + plat.getDest().h > y2)
					y2 = plat.getPosition().y + plat.getDest().h;
			}
		}
		
		if (type == PlatformGroup.HORIZONTAL)
		{
			dim.w =(int)(x2 - x1);
			//dim.h já é do tamanho de camera.viewportHeight
		}
		else
		{
			//dim.w já é do tamanho de camera.viewportWidth
			dim.h = (int)(y2 - y1);
		}
	}
	
	public void setPosition ( Vector2 p ){
		pos.x = p.x;
		pos.y = p.y;
		dim.x = (int)p.x;
		dim.y = (int)p.y;
		
		for (Platform plat: platforms)
		{
			p.x = plat.getInitPosition().x + pos.x;
			p.y = plat.getInitPosition().y + pos.y;
			if (plat.getState() != Platform.FALLING)
				plat.setPosition(p);
		}
	}
	
	public void reset (  ) {
		if (reseted == false)
		{
			for (Platform plat: platforms)
				plat.reset(getFishes());//antes era reset normal e peixes aleatorios
			reseted = true;
		}
	}
	
	public void loadPlatforms ( OrthographicCamera camera, Texture texture, String path ) {
		// remove as plataformas anteriores
		dispose();
		
		JsonReader reader = new JsonReader();
		JsonValue value, child = reader.parse(Gdx.files.internal(path));
		
		for (value = child.child; value != null; value = value.next)
		{
			if (value.name.equals("fishes"))
			{
				grayFish = value.child.get("grayFish").asInt();
				greenFish = value.child.get("greenFish").asInt();
				redFish = value.child.get("redFish").asInt();
				Gdx.app.log("PlatformGroup","leu fishes");
				continue;
			}
			
			Object[] atts = {
					value.name,//type
					value.child.get("fish").asString(),
					value.child.get("x").asInt(),
					value.child.get("y").asInt(),
			};
			
			int type = -1;
			type = Platform.getTypeByName((String)atts[0]);
			
			int fishType = -1;
			fishType = Fish.getTypeByName((String)atts[1]);
			// não adicionar plataformas diretamente no platforms mas somente usando o método abaixo
			addPlatform(new Platform(camera,texture,type,fishType, (Integer)atts[2],(Integer)atts[3]));
		}
	}
	
	public void dispose (  ) {
		for (Platform plat: platforms)
			plat.dispose();
	}
	
	@Override
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		// desenha as plataformas
		int i = 0;
		for (Platform p: platforms)
		{
			if (i == 0)
			{
				i++;
				batch.draw(pixelR, p.getPosition().x, p.getPosition().y, p.getDest().w,p.getDest().h);
			}
			
			p.draw(batch, camera);
		}
		
		// desenha os peixes
		for (Platform p: platforms)
			for (Fish fish: p.getFishes())
				fish.draw(batch, camera);
	}
	
	@Override
	public int update (  ) {
		for (Platform plat: platforms)
			plat.update();
		
		return getState();
	}
}
