package com.game.vuxjump.elements;

import java.util.Vector;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;
// obstaculo da fase do skate
public class RoadObstacle extends StateMachine {
	Vector<Fish> fishes;
	Rect collRect;//retangulo de colisão
	Rect dest;//dimensão da imagem do obstáculo
	Vector2 initPos; //posição setada ao criar o obstáculo
	
	public RoadObstacle ( OrthographicCamera camera, Texture texture, int type, int fishType, Integer initX, Integer initY ) {
		
	}

	public void dispose() {
		
	}
	
	public Rect getCollRect() {
		return collRect;
	}

	public static int getTypeByName ( String string ) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Rect getDest (  ) {
		return dest;
	}

	public Vector2 getInitPosition (  ) {
		// TODO Auto-generated method stub
		return initPos;
	}

	@Override
	public int update (  ) {
		return getState();
	}

	public Vector<Fish> getFishes() {
		return fishes;
	}
}
