package com.game.vuxjump.elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.game.vuxjump.StateMachine;

/*
 * Classe para ser a imagem que aparece quando o player sai pela parte superior da tela,
 * e ela fica embaixo na visualização indicando a posição do player
 */

public class PlayerTarget extends StateMachine {
	public static final int INACTIVE_STATE = 0;
	public static final int VISIBLE_STATE = 1;
	private Texture texture;
	private TextureRegion textureRegion;
	private Player player;
	public PlayerTarget ( Player player ) {
		this.player = player;
		texture = new Texture(Gdx.files.internal("player/"+player.getName()+"/playerPos.png"));
		textureRegion = new TextureRegion(texture);
		textureRegion.flip(false, true);
		setState(INACTIVE_STATE);
	}
	
	public void dispose (  ) {
		texture.dispose();
	}
	
	@Override
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		if (getState() == VISIBLE_STATE)
			batch.draw(textureRegion, pos.x, pos.y);
	}
	
	@Override
	public int update (  ) {
		if (player.getPosition().y < -player.getDim().h/2)
		{
			setState(VISIBLE_STATE);
			pos.x = player.getPosition().x;
			pos.y = 0;
		}
		else
			setState(INACTIVE_STATE);
		
		return getState();
	}
}
