package com.game.vuxjump.elements;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;
import com.game.vuxjump.hud.LevelHUD;

public class Fish extends StateMachine {
	public static final int STOP_STATE = 0;
	public static final int TOHUD_STATE = 1;
	public static final int HIDE_STATE = 2;
	
	public static final int GRAY = 0;
	public static final int GREEN = 1;
	public static final int RED = 2;
	public static final int BLUE = -3; // em breve
	public static final int MAX_FISH = 3; // muda o valor toda vez que adiciona um novo tipo de peixe
	
	private int value; // o quanto vale no score esse peixe
	private int type;
	private boolean visible;
	private Rect collRect;
	private Rect dim;
	private Vector2 vel, acc;
	private Vector2 posGUI;
	private Rect hudPos; // posição final do peixe no hud 
	private OrthographicCamera camera;
	
	private Texture texture;
	private TextureRegion textureRegion;
	private static HashMap<Integer, Texture> textures = new HashMap<Integer,Texture>();;
	
	public Fish ( OrthographicCamera camera, int type, int x, int y ) {
		setType(type);
		collRect = new Rect();
		dim = new Rect();
		vel = new Vector2();
		acc = new Vector2(10,10);
		posGUI = new Vector2();
		hudPos = new Rect();
		pos.x = x;
		pos.y = y;
		posGUI.set(pos);
		this.type = type;
		value = 1;
		visible = true;
		this.camera = camera;
		collRect = new Rect(x,y,textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
		dim = new Rect(0,0,textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
		setState(STOP_STATE);
	}
	
	static boolean destroy = true;
	public void dispose (  ) {
		if (destroy)
		{
			for (int i = 0; i < MAX_FISH; i++)
				if (textures.get(i) != null)
					textures.get(i).dispose();
			textures.clear();
			destroy = false;
		}
	}
	
	public static int getTypeByName ( String type ) {
		if (type.equals("gray"))
			return Fish.GRAY;
		else if (type.equals("green"))
			return Fish.GREEN;
		else if (type.equals("red"))
			return Fish.RED;
		else if (type.equals("blue"))
			return Fish.BLUE;
		else if (type.equals("random"))
			return (int)(Fish.MAX_FISH*Math.random() + Fish.GRAY);
		
		
		return -1;
	}
	
	public void setType ( int t ) {
		type = t;
		
		
		if (textures.containsKey(type))
			texture = textures.get(type);
		else
		{
			switch (type)
			{
				case GRAY:
					texture = new Texture(Gdx.files.internal("fish0.png"));
					break;
				case GREEN:
					texture = new Texture(Gdx.files.internal("fish1.png"));
					break;
				case RED:
					texture = new Texture(Gdx.files.internal("fish2.png"));
					break;
				default:
					this.type = GRAY;
					texture = new Texture(Gdx.files.internal("fish0.png"));
					type = GRAY;
					break;
			}
			textures.put(type,texture);
		}
		
		textureRegion = new TextureRegion(texture);
		boolean flipX = (Math.random() < 0.5);
		textureRegion.flip(flipX, true);
	}
	
	public Rect getCollRect (  ) {
		return collRect; 
	}
	
	public void hide (  ) {
		visible = false;
	}
	
	public void unhide (  ) {
		visible = true;
	}
	
	public boolean isVisible (  ) {
		return visible;
	}
	
	public int getValue (  ) {
		return value;
	}
	
	public int getType (  ) {
		return type;
	}
	
	public void setDim ( Rect d ) {
		dim.set(d);
	}
	
	public Texture getTexture (  ) {
		return texture;
	}
	
	public TextureRegion getTextureRegion (  ) {
		return textureRegion;
	}
	
	public void gotoHUD (  ) {
		if (getState() == STOP_STATE)
		{
			Vector2 aux, hPos = new Vector2(camera.position.x + LevelHUD.instance().getFishPos(type).x, LevelHUD.instance().getFishPos(type).y);
			aux = hPos.sub(pos);
			aux.nor();
			
			//posiciona o peixe do hud
			posGUI.set(pos);
			vel.x = aux.x * acc.x;
			vel.y = aux.y * acc.y;
			setState(TOHUD_STATE);
		}
	}
	
	public void setPosition ( Vector2 p ) {
		collRect.x = (int)p.x;
		collRect.y = (int)p.y;
		if (getState() != TOHUD_STATE)
			posGUI.set(pos);
		super.setPosition(p);
	}
	
	public void reset ( Vector2 p ) {
		vel.setZero();
		setPosition(p);
		boolean flipX = (Math.random() < 0.5);
		textureRegion.flip(flipX, false); // flipY = false pois lá em no construtor foi true.
		setState(STOP_STATE);
	}
	
	@Override
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		if (getState() == STOP_STATE)
		{
			batch.draw(textureRegion, pos.x, pos.y, dim.w, dim.h);
		}
		else if (getState() == TOHUD_STATE)
		{
			batch.draw(textureRegion, posGUI.x, posGUI.y, dim.w, dim.h);
		}
	}
	
	@Override
	public int update (  ) {
		switch (getState())
		{
			case STOP_STATE: 
			case HIDE_STATE:
				break;
			case TOHUD_STATE:
				Vector2 aux, hPos = new Vector2(LevelHUD.instance().getFishPos(type));
				aux = hPos.sub(pos);
				aux.nor();
				
				vel.x = aux.x * acc.x;
				vel.y = aux.y * acc.y;
				
				posGUI.x += vel.x;
				posGUI.y += vel.y;
				
				hudPos.set((int)(LevelHUD.instance().getFishPos(type).x) - 16, (int)(LevelHUD.instance().getFishPos(type).y) - 32, 32, 64);
				collRect.x = (int)posGUI.x;
				collRect.y = (int)posGUI.y;
				if (collRect.boundingbox(hudPos))
				{
					hide();
					LevelHUD.instance().doRun(getType());
					//Gdx.app.log("Fish","Encerrando peixe no ponto hudPos");
					setState(HIDE_STATE);
				}
		}
		return getState();
	}
}
