package com.game.vuxjump;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class ScrolledBackground {
	private OrthographicCamera camera;
	protected Vector2 vel;
	protected Vector2 pos;
	protected Texture texture;
	protected int width, height;
	protected boolean destroyTexture = true;
	
	/*
	 * Com este construtor é possivel setar uma textura mas não será destruida quando chamar BG.dispose
	 */
	public ScrolledBackground ( OrthographicCamera camera, Texture texture, int w, int h ) {
		this.camera = camera;
		this.width = w;
		this.height = h;
		vel = new Vector2();
		pos = new Vector2(camera.position.x - camera.viewportWidth/2,camera.position.y - camera.viewportHeight/2);
		this.texture = texture;
		destroyTexture = false;
		texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
	}
	
	public ScrolledBackground ( OrthographicCamera camera, String path, int w, int h ) {
		this.camera = camera;
		this.width = w;
		this.height = h;
		vel = new Vector2();
		pos = new Vector2(camera.position.x - camera.viewportWidth/2,camera.position.y - camera.viewportHeight/2);
		texture = new Texture(Gdx.files.internal(path));
		texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
	}
	
	public void dispose (  ) {
		if (destroyTexture)
			texture.dispose();
	}
	
	public void setPos ( Vector2 p ) {
		pos.set(p);
	}
	
	public Vector2 getPos (  ) {
		return pos;
	}
	
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		batch.draw(texture, pos.x, pos.y, (float)camera.viewportWidth, (float)camera.viewportHeight,(int)pos.x, (int)pos.y, width,height, false, true);
	}
	
	// desenha na tela sem 'pos' ser usado como destino
	public void drawZero ( SpriteBatch batch, OrthographicCamera camera ) {
		batch.draw(texture, 0, 0, (float)camera.viewportWidth, (float)camera.viewportHeight,(int)pos.x, (int)pos.y, width,height, false, true);
	}
	
	public void update (  ) {
		pos.x = camera.position.x - camera.viewportWidth/2.0f;
		pos.y = camera.position.y - camera.viewportHeight/2.0f;
	}
}
