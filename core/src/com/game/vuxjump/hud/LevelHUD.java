package com.game.vuxjump.hud;

import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StateMachine;
import com.game.vuxjump.Writer;
import com.game.vuxjump.elements.Fish;
import com.game.vuxjump.elements.Player;
import com.game.vuxjump.elements.SkaterPlayer;

public class LevelHUD extends StateMachine {
	public static final int STOP_FISH = 0;
	public static final int RUN_FISH = 1;
	
	private Vector<Texture> texture;
	private Vector<TextureRegion> fishHUD;
	private Rect fishSize;
	private Player player; 
	private SkaterPlayer skater; 
	private boolean[] visible;
	private Color[] color; 
	private Vector<Vector2> fishPos;
	private static LevelHUD singleton = null; 
	private OrthographicCamera camera;
	private Viewport viewport;
	
	private LevelHUD (  ) {
		camera = null;
		
		texture = new Vector<Texture>();
		texture.add(new Texture(Gdx.files.internal("gui/fish0.png")));
		texture.add(new Texture(Gdx.files.internal("gui/fish1.png")));
		texture.add(new Texture(Gdx.files.internal("gui/fish2.png")));
		fishSize = new Rect(0,0,80,80);
		
		fishHUD = new Vector<TextureRegion>();
		TextureRegion region = new TextureRegion(texture.get(0));
		region.flip(false, true);
		fishHUD.add(region);
		region = new TextureRegion(texture.get(1));
		region.flip(false, true);
		fishHUD.add(region);
		region = new TextureRegion(texture.get(2));
		region.flip(false, true);
		fishHUD.add(region);
		
		fishPos = new Vector<Vector2>();
		fishPos.add(new Vector2(32,32));
		fishPos.add(new Vector2(32,32+fishSize.h));
		fishPos.add(new Vector2(32,32+fishSize.h*2));
		color = new Color[]{
				new Color((float)0xbe/0xff,(float)0xb6/0xff,(float)0xa9/0xff, 1f),
				new Color((float)31/0xFF,(float)0xcd/0xFF, (float)0xb2/0xFF, 1f),
				new Color(1f,0,0,1f),
				};
		visible = new boolean[]{false, false, false};
		setPosition(fishPos.get(Fish.GRAY));
		Writer.instance().loadFont("font50", "font50.json", "font50.png");;
	}
	
	
	public static synchronized LevelHUD instance (  ) {
		if (singleton == null)
			singleton = new LevelHUD(); 
		return singleton;
	}
	
	public TextureRegion getFishHUD ( int fishType ) {
		return fishHUD.get(fishType);
	}
	
	// faz peixe do tipo passado ficar visivel
	public void doRun ( int fishType ) {
		visible[fishType] = true;
	}
	
	public Vector2 getFishPos ( int fishType ) {
		return fishPos.get(fishType);
	}
	
	public void setCamera ( OrthographicCamera camera ) {
		this.camera = camera;
	}
	
	public void updateViewport ( int w, int h ) {
		viewport.update(w,h);
	}
	
	public void setPlayer ( Player player ) {
		this.player = player;
		this.skater = null;
	}
	
	public void setSkater ( SkaterPlayer skater ) {
		this.skater = skater;
		this.player = null;
	}
	
	public void destroy (  ) {
		for (Texture tex: texture)
			tex.dispose();
	}
	
	public static void dispose (  ) {
		if (instance() != null)
			instance().destroy();
	}
	
	public void reset (  ) {
		visible = new boolean[]{false, false, false};
		fishPos.clear();
		fishPos.add(new Vector2(32,32));
		fishPos.add(new Vector2(32,32+fishSize.h));
		fishPos.add(new Vector2(32,32+fishSize.h*2));
	}
	
	@Override
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		for (int i = Fish.GRAY; i < Fish.MAX_FISH; i++)
			if (visible[i])
			{
				int score = 0;
				if (player != null)
					score = player.getScore(i);
				else if (skater != null)
					score = skater.getScore(i);
				
				batch.draw(fishHUD.get(i), fishPos.get(i).x, fishPos.get(i).y, fishSize.w, fishSize.h);
				Writer.instance().renderText(batch, "font50", color[i], fishPos.get(i).x + (float)fishSize.w, fishPos.get(i).y + (float)(fishSize.h/2), "X"+score);
			}
	}
	
	public int update (  ) {
		
		for (int i = Fish.GRAY; i < Fish.MAX_FISH; i++)
		{
			fishPos.get(i).x = (camera.position.x - camera.viewportWidth/2.0f) + 32.0f;
			fishPos.get(i).y = 32.0f + (float)(fishSize.h * i) + (camera.position.y - camera.viewportHeight/2);
		}
		
		return getState();
	}

}
