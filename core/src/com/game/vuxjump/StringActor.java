package com.game.vuxjump;


import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;


public class StringActor extends Actor implements Disposable{
	private Font font = null;
	private String string;
	private Color color;
	
	public StringActor (String fontName, String fileFontPath, String fontImagePath, String str ){
		font = new Font();
		loadFont(fontName, fileFontPath, fontImagePath);
		setColor(Color.WHITE);
		setString(str);
	}
	
	
	public StringActor (String fontName, String fileFontPath, Texture texture, String str ){
		font = new Font();
		loadFont(fontName, fileFontPath, texture);
		setColor(Color.WHITE);
		setString(str);
	}
	
	
	public String getString() {
		return string;
	}



	public void setString(String str) {
		string = str;
	}

	@Override
	public void setColor(Color c){
		color = c;
	}

	public Color getColor(){
		return color;
	}

	@Override
	public void dispose (  ){
		if(font != null)
			font.dispose();
		
	}
	
	public class Font {
		public String fontName;
		public Map<Character, TextureRegion> letter;
		public Texture texture;
		public float charSpace;
		public boolean destroyTexture;
		
		public Font (  ){
			charSpace = 0;
			fontName = "none";
			letter = new HashMap<Character, TextureRegion>();
			texture = null;
			destroyTexture = true;
		}
		
		public void dispose (  ){
			letter.clear();
			if (destroyTexture)
				texture.dispose();
		}
	}
	
	
	public void flip ( boolean flipX, boolean flipY ) {
		for (Map.Entry<Character, TextureRegion> entry: font.letter.entrySet())
				entry.getValue().flip(flipX, flipY);
	}
	
	private void loadFont ( String fontName, String fileFontPath, String fontImagePath ){
		font.texture = new Texture(Gdx.files.internal(fontImagePath));
		font.fontName = fontName;
		
		JsonReader reader = new JsonReader();
		JsonValue value, child = reader.parse(Gdx.files.internal(fileFontPath));
		
		value = child.child; 
		while(value != null){
			int[] atts = {
					value.child.get("x").asInt(),
					value.child.get("y").asInt(),
					value.child.get("w").asInt(),
					value.child.get("h").asInt()
			};

			TextureRegion region = new TextureRegion(font.texture, atts[0],atts[1],atts[2],atts[3]);
			region.flip(false, true);
			font.letter.put(value.name.charAt(0), region);
			value = value.next;
		}
	}
	
	private void loadFont ( String fontName, String fileFontPath, Texture texture ){
		font.texture = texture;
		font.fontName = fontName;
		font.destroyTexture = false;
		
		JsonReader reader = new JsonReader();
		JsonValue value, child = reader.parse(Gdx.files.internal(fileFontPath));
		
		value = child.child; 
		while(value != null){
			int[] atts = {
					value.child.get("x").asInt(),
					value.child.get("y").asInt(),
					value.child.get("w").asInt(),
					value.child.get("h").asInt()
			};

			TextureRegion region = new TextureRegion(font.texture, atts[0],atts[1],atts[2],atts[3]);
			region.flip(false, true);
			font.letter.put(value.name.charAt(0), region);
			value = value.next;
		}
	}
	
	@Override
	public void draw (Batch batch, float parentAlpha){
		TextureRegion region = null;
		
		int count = 0;
		
		for (int i = 0; i < string.length(); i++){
			if(string.charAt(i) == '\n'){
				count++;
			}
		}
		
		float px = getX(), py = getY() + (font.letter.get('a').getRegionHeight() * count);
		
		for (int i = 0; i < string.length(); i++){
			if (string.charAt(i) == ' ')
				px += font.charSpace + font.letter.get('a').getRegionWidth();
			else if (string.charAt(i) == '\n'){
				px = getX();
				py -= font.letter.get('a').getRegionHeight();
			}
					
			for (Map.Entry<Character,TextureRegion> entry: font.letter.entrySet()){
				if (entry.getKey() == (Character)string.charAt(i)){
					
					region = entry.getValue();
					
					batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);							
					
					batch.draw(region, px, py, getOriginX(), getOriginY(), region.getRegionWidth(), region.getRegionHeight(), getScaleX(), getScaleY(), getRotation());
					
					px += font.charSpace + region.getRegionWidth() * getScaleX();
				}
			}
		}
	}
	
	public Font getFont(){		
		return font;
	}
	
	@Override
	public float getWidth(){
		int w = 0;
		
		for (int i = 0, k = 0; i < string.length(); i++)
		{
			k = 0;
			for (int j = i; j < string.length() && string.charAt(j ) != '\n'; j++)
				k++;
			
			if (k > w)
				w = k;
		}
		
		w *= font.letter.get('a').getRegionWidth();

		
		return w;
	}
	
	@Override
	public float getHeight(){
		int h = 0;
		int count = 1;
		for(int i = 0; i < string.length(); i++){
			if(string.charAt(i) == '\n'){
				count += 1; 
			}
		}
		h = font.letter.get(string.charAt(0)).getRegionHeight() * count; 
		return h;
	}
	
	
}