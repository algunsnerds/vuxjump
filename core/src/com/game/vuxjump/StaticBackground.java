package com.game.vuxjump;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class StaticBackground {
	private OrthographicCamera camera;
	protected Vector2 vel;
	protected Vector2 pos;
	protected Texture texture;
	protected TextureRegion region;
	protected int width, height;
	
	public StaticBackground ( OrthographicCamera camera, String path, int w, int h ) {
		this.camera = camera;
		this.width = w;
		this.height = h;
		vel = new Vector2();
		pos = new Vector2(camera.position.x - camera.viewportWidth/2,camera.position.y - camera.viewportHeight/2);
		texture = new Texture(Gdx.files.internal(path));
		region = new TextureRegion(texture, 0,0,w,h);
		region.flip(false, true);
		//texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
	}
	
	public void dispose (  ) {
		texture.dispose();
	}
	
	public TextureRegion getTextureRegion (  ) {
		return region;
	}
	
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		batch.draw(region, pos.x, pos.y, (float)camera.viewportWidth, (float)camera.viewportHeight);
	}
	
	public void draw ( SpriteBatch batch, OrthographicCamera camera, float x, float y ) {
		batch.draw(region, pos.x+x, pos.y+y, (float)camera.viewportWidth, (float)camera.viewportHeight);
	}
	
	public void draw ( SpriteBatch batch, OrthographicCamera camera, float x, float y, float w, float h ) {
		batch.draw(texture, pos.x+x, pos.y+y, w, h, 0,0,width,height,false,true);
	}
	
	public void update (  ) {
		pos.x = camera.position.x - camera.viewportWidth/2.0f;
		pos.y = camera.position.y - camera.viewportHeight/2.0f;
	}

	public Texture getTexture() {
		return texture;
	}
}
