package com.game.vuxjump.minigame;

import java.util.Vector;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.game.vuxjump.GameState;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StaticBackground;
import com.game.vuxjump.gui.LevelGui;
import com.game.vuxjump.screen.GameScreen;

public class Snake implements GameScreen {
	public static final int UP = 0;
	public static final int DOWN = 1;
	public static final int RIGHT = 2;
	public static final int LEFT = 3;
	
	int score;
	Vector<Piece> body;
	Vector<Piece> apple;
	boolean eatSnake;
	float mapWidth;
	float mapHeight;
	float mapStartW, mapStartH;
	float pixelSize; // usado para posicionar as imagens antes de desenhar na tela
	Vector2 vel; // 
	int currSize;
	
	// na sequencia:  UP,DOWN,RIGHT,LEFT
	boolean[] keys = {false,false,false,false}; 
	
	Vector<Vector2> eatenPos;
	Vector<Rect> excludedArea;
	
	Texture appleTexture;
	TextureRegion appleRegion;
	TextureRegion lineRegion0, lineRegion1;
	TextureRegion cornerRegion0, cornerRegion1, cornerRegion2, cornerRegion3;
	TextureRegion tailRegion0,tailRegion1,tailRegion2,tailRegion3;
	TextureRegion headRegion0,headRegion1,headRegion2,headRegion3;
	TextureRegion eatenRegion;
	TextureRegion eatenLineRegion0, eatenLineRegion1;
	TextureRegion eatenCornerRegion0, eatenCornerRegion1, eatenCornerRegion2, eatenCornerRegion3;
	
	StaticBackground background;
	
	OrthographicCamera camera;
	Viewport viewport;
	
	protected LevelGui gui;
	
	public Snake (  ) {
		
		score = 0;
		body = new Vector<Piece>();
		apple = new Vector<Piece>();
		apple.add(new Piece(0,0,0));
		eatSnake = false;
		vel = new Vector2(); 
		
		appleTexture = new Texture(Gdx.files.internal("snake/apple.png"));
		appleRegion = new TextureRegion(appleTexture); 
		appleRegion.flip(false, true);
		
		/////Snakes///////
		lineRegion0 = new TextureRegion(new Texture(Gdx.files.internal("snake/pieceLine0.png")));
		lineRegion0.flip(false,true);
		lineRegion1 = new TextureRegion(new Texture(Gdx.files.internal("snake/pieceLine1.png")));
		lineRegion1.flip(false,true);
		cornerRegion0 = new TextureRegion(new Texture(Gdx.files.internal("snake/pieceCorner0.png")));
		cornerRegion0.flip(false,true);
		cornerRegion1 = new TextureRegion(new Texture(Gdx.files.internal("snake/pieceCorner1.png")));
		cornerRegion1.flip(false,true);
		cornerRegion2 = new TextureRegion(new Texture(Gdx.files.internal("snake/pieceCorner2.png")));
		cornerRegion2.flip(false,true);
		cornerRegion3 = new TextureRegion(new Texture(Gdx.files.internal("snake/pieceCorner3.png")));
		cornerRegion3.flip(false,true);
		
		headRegion0 = new TextureRegion(new Texture(Gdx.files.internal("snake/head0.png")));
		headRegion0.flip(false,true);
		headRegion1= new TextureRegion(new Texture(Gdx.files.internal("snake/head1.png")));
		headRegion1.flip(false,true);
		headRegion2 = new TextureRegion(new Texture(Gdx.files.internal("snake/head2.png")));
		headRegion2.flip(false,true);
		headRegion3 = new TextureRegion(new Texture(Gdx.files.internal("snake/head3.png")));
		headRegion3.flip(false,true);
		
		tailRegion0 = new TextureRegion(new Texture(Gdx.files.internal("snake/tail0.png")));
		tailRegion0.flip(false,true);
		tailRegion1= new TextureRegion(new Texture(Gdx.files.internal("snake/tail1.png")));
		tailRegion1.flip(false,true);
		tailRegion2 = new TextureRegion(new Texture(Gdx.files.internal("snake/tail2.png")));
		tailRegion2.flip(false,true);
		tailRegion3 = new TextureRegion(new Texture(Gdx.files.internal("snake/tail3.png")));
		tailRegion3.flip(false,true);
		
		eatenRegion = new TextureRegion(new Texture(Gdx.files.internal("snake/eaten.png")));
		eatenRegion.flip(false,true);
		
		eatenPos = new Vector<Vector2>();
		excludedArea = new Vector<Rect>();
		
		eatenLineRegion0 = new TextureRegion(new Texture(Gdx.files.internal("snake/eaten0.png")));
		eatenLineRegion0.flip(false,true);
		eatenLineRegion1 = new TextureRegion(new Texture(Gdx.files.internal("snake/eaten1.png")));
		eatenLineRegion1.flip(false,true);
		eatenCornerRegion0 = new TextureRegion(new Texture(Gdx.files.internal("snake/eatenCorner0.png")));
		eatenCornerRegion0.flip(false,true);
		eatenCornerRegion1 = new TextureRegion(new Texture(Gdx.files.internal("snake/eatenCorner1.png")));
		eatenCornerRegion1.flip(false,true);
		eatenCornerRegion2 = new TextureRegion(new Texture(Gdx.files.internal("snake/eatenCorner2.png")));
		eatenCornerRegion2.flip(false,true);
		eatenCornerRegion3 = new TextureRegion(new Texture(Gdx.files.internal("snake/eatenCorner3.png")));
		eatenCornerRegion3.flip(false,true);
		
		//GameState.setOrientation("portrait");
		if (GameState.getOrientationValue().equals("landscape"))
		{
			camera = new OrthographicCamera(1024,600);
			camera.setToOrtho(true, 1024,600);
			viewport = new StretchViewport(1024,600,camera);
			mapStartW = mapStartH = 0;
		}
		else
		{
			camera = new OrthographicCamera(600,1024);
			camera.setToOrtho(true, 600,1024);
			viewport = new StretchViewport(600,1024,camera);
			mapStartW = mapStartH = 0;
		}
		
		background = new StaticBackground(camera,"snake/bg.png",512,512);
		
		pixelSize = 32;
		mapWidth = (int)(camera.viewportWidth/pixelSize) - mapStartW;
		mapHeight = (int)(camera.viewportHeight/pixelSize) - mapStartH;
		
		if (camera.viewportWidth % pixelSize > pixelSize/2)
		{
			Gdx.app.log("Snake","antes map.W = "+mapWidth);
			mapWidth++;
		}
		
		if (camera.viewportHeight % pixelSize > pixelSize/2)
		{
			Gdx.app.log("Snake","antes map.H = "+mapHeight);
			mapHeight++;
		}
		
		startGame();
		gui = new LevelGui(this);
		excludedArea.add(gui.getPauseRect());
		Gdx.app.log("Snake","mapW="+mapWidth+" mapH="+mapHeight);
	}
	
	public class Piece {
		public Vector2 pos;
		public int dir;
		
		public Piece ( float x, float y, int d ) {
			pos = new Vector2(x, y);
			dir = d;
		}
		
		public void randomPos (  ) {
			pos.x = (int)(Math.random() * mapWidth);
			pos.y = (int)(Math.random() * mapHeight);
			dir = -1;
		}
	}
	
	@Override
	public void dispose() {
		gui.dispose();
		appleTexture.dispose();
		lineRegion0.getTexture().dispose();
		lineRegion1.getTexture().dispose();
		cornerRegion0.getTexture().dispose();
		cornerRegion1.getTexture().dispose();
		cornerRegion2.getTexture().dispose();
		cornerRegion3.getTexture().dispose();
		
		headRegion0.getTexture().dispose();
		headRegion1.getTexture().dispose();
		headRegion2.getTexture().dispose();
		headRegion3.getTexture().dispose();
		
		tailRegion0.getTexture().dispose();
		tailRegion1.getTexture().dispose();
		tailRegion2.getTexture().dispose();
		tailRegion3.getTexture().dispose();
		
		eatenRegion.getTexture().dispose();
		eatenLineRegion0.getTexture().dispose();
		eatenLineRegion1.getTexture().dispose();
		eatenCornerRegion0.getTexture().dispose();
		eatenCornerRegion1.getTexture().dispose();
		eatenCornerRegion2.getTexture().dispose();
		eatenCornerRegion3.getTexture().dispose();
		
		background.dispose();
	}
	
	boolean inited = false;
	@Override
	public void show() {
		if (inited)
		{
			gui.dispose();
			gui = new LevelGui(this);
			reset();
			return;
		}
		
		inited = true;
	}

	float time = 0;
	Vector2 dest = new Vector2();
	@Override
	public void render(float delta) {
		if (gui.isPaused() == false) {
			time += delta;
			if (time > 0.300)
			{
				time = 0;
				
				moveSnake();
				
				boolean lock = false;
				// colisão com a maçã
				for (int i = 0; i < apple.size(); i++)
				{
					Piece p = apple.get(i);
					if ((body.lastElement().pos.x == p.pos.x) && (body.lastElement().pos.y == p.pos.y))
					{
						eatenPos.add(new Vector2(body.lastElement().pos));
						body.add(new Piece(body.lastElement().pos.x, body.lastElement().pos.y, body.lastElement().dir));
						moveSnake();
						// reinicializando a posição da maçã
						// escolhe uma posição diferente das peças da serpente
						setApplePos(p);
						i = 0;
						
						// adiciona mais maçãs se possivel
						if (lock == false && Math.random() < 0.5f && apple.size() < mapWidth*mapHeight*0.1f)
						{
							lock = true;//evita ficar readicionando maçãs
							apple.add(new Piece(0,0,0));
							boolean equals = false;
							do
							{
								equals = false;
								setApplePos(apple.lastElement());
								for (Piece q: apple)
								{
									if (q != apple.lastElement())
										if (q.pos.x == apple.lastElement().pos.x &&
										    q.pos.y == apple.lastElement().pos.y)
										{
											equals = true;
											break;
										}
								}
							} while(equals);
							
							if ((body.lastElement().pos.x == apple.lastElement().pos.x) && (body.lastElement().pos.y == apple.lastElement().pos.y))
							{
								eatenPos.add(new Vector2(body.lastElement().pos));
								body.add(new Piece(body.lastElement().pos.x, body.lastElement().pos.y, body.lastElement().dir));
								moveSnake();
								setApplePos(apple.lastElement());
							}
						}
					}
				}
				
				for (int i = 0; i < eatenPos.size(); i++)
				{
					if ((body.get(0).pos.x == eatenPos.get(i).x && body.get(0).pos.y == eatenPos.get(i).y) ||
					     (!hasBodyPiece(eatenPos.get(i).x,eatenPos.get(i).y-1)&&
					      !hasBodyPiece(eatenPos.get(i).x,eatenPos.get(i).y+1)&&
					      !hasBodyPiece(eatenPos.get(i).x-1,eatenPos.get(i).y)&&
					      !hasBodyPiece(eatenPos.get(i).x+1,eatenPos.get(i).y)))
					{
						eatenPos.remove(i);
						i = 0;
						if (eatenPos.size() == 0)
							break;
					}
				}
				
				
				
				if (eatSnake)
				{
					apple.clear();
					apple.add(new Piece(0,0,0));
					startGame();
					eatSnake = false;
					eatenPos.clear();
				}
			}
		}
		// verifica se mordeu uma parte do corpo
		for (int i = 0; i < body.size() - 2 && eatSnake == false; i++)
		{
			if (body.lastElement().pos.x == body.get(i).pos.x && body.lastElement().pos.y == body.get(i).pos.y)
				eatSnake = true;
		}
		
		
		Gdx.gl.glClearColor(1,1,1,1);
		Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
		SpriteBatch batch = GameState.instance().getBatch();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			background.draw(batch, camera, mapStartW*pixelSize,mapStartH*pixelSize,mapWidth*pixelSize,mapHeight*pixelSize);
			drawSnake(batch);
		batch.end();
		
		gui.draw();
	}
	
	public void reset (  ) {
		
	}
	
	// controles da cobra
	@Override
	public boolean fling ( float vx, float vy, int button ) {
		float keyX = Math.abs(vx);
		float keyY = Math.abs(vy);
		
		if (keyX > keyY)
		{
			if (vx > 0)
			{
				keys[RIGHT] = true;
				keys[LEFT] = false;
				keys[UP] = false;
				keys[DOWN] = false;
			}
			else
			{
				keys[RIGHT] = false;
				keys[LEFT] = true;
				keys[UP] = false;
				keys[DOWN] = false;
			}
		}
		else if (keyX < keyY)
		{
			if (vy > 0)
			{
				keys[RIGHT] = false;
				keys[LEFT] = false;
				keys[DOWN] = true;
				keys[UP] = false;
			}
			else
			{
				keys[RIGHT] = false;
				keys[LEFT] = false;
				keys[DOWN] = false;
				keys[UP] = true;
			}
		}

		return false;
	}
	
	boolean hasBodyPiece ( float x, float y ) {
		for (Piece p: body)
			if (p.pos.x == x && p.pos.y == y)
				return true;
		return false;
	}
	
	public void moveSnake (  ) {
		if (keys[RIGHT] && body.lastElement().dir != LEFT)
		{
			vel.x = 1; // move horizontalmente a cabeça para direita
			vel.y = 0; // e para de mover verticalmente
			body.lastElement().dir = RIGHT;
		}
		else if (keys[LEFT] && body.lastElement().dir != RIGHT)
		{
			vel.x = -1;  // move horizontalmente a cabeça para esquerda
			vel.y = 0; // e para de mover verticalmente
			body.lastElement().dir = LEFT;
		}
		else if (keys[UP] && body.lastElement().dir != DOWN)
		{
			vel.x = 0; // para de mover horizontalmente
			vel.y = -1;  // e move verticalmente a cabeça
			body.lastElement().dir = UP;
		}
		else if (keys[DOWN] && body.lastElement().dir != UP)
		{
			vel.x = 0; // para de mover horizontalmente
			vel.y = 1; // e move verticalmente a cabeça
			body.lastElement().dir = DOWN;
		}

		// depois ajustando as posições das outras peças (partes)
		// primeiro move as partes do corpo
		if (vel.x != 0 || vel.y != 0) // se estiver movendo
		{
			for (int i = 0; i < body.size() - 1; i++)
			{
				// faça a peça de trás (pedaco[i]) igual a peça da frente (pedaco[i + 1])
				body.get(i).pos.x = body.get(i + 1).pos.x;
				body.get(i).pos.y = body.get(i + 1).pos.y;
				body.get(i).dir = body.get(i + 1).dir;
			}
		}
		// agora move a cabeça
		body.lastElement().pos.x += vel.x;
		body.lastElement().pos.y += vel.y;

		// Verifica os limites do movimento da cobra
		// Para o eixo X
		// se estiver além da largura da tela/mapa
		if (body.lastElement().pos.x >= mapWidth + mapStartW)
		{
			// volte para posição coorX = 0
			body.lastElement().pos.x = mapStartW;
		}
		else if (body.lastElement().pos.x < mapStartW) // se estiver além de 0
		{
			// volte para a posição da largura do mapa
			body.lastElement().pos.x = mapStartW + mapWidth - 1;
		}

		// Para o eixo Y
		if (body.lastElement().pos.y >= mapHeight + mapStartH)
		{
			body.lastElement().pos.y = mapStartH;
		}
		else if (body.lastElement().pos.y < mapStartH)
		{
			body.lastElement().pos.y = mapStartH + mapHeight - 1;
		}
	}
	
	public void drawSnake ( SpriteBatch batch ) {
		// desenha o corpo
		drawBody(batch);
		
		// desenha o rabo
		dest.set(body.get(0).pos.x * pixelSize, body.get(0).pos.y * pixelSize);
		
		switch (body.get(0).dir)
		{
			case UP:
				batch.draw(tailRegion0,dest.x,dest.y,pixelSize,pixelSize);
				break;
			case DOWN:
				batch.draw(tailRegion2,dest.x,dest.y,pixelSize,pixelSize);
				break;
			case RIGHT:
				batch.draw(tailRegion1,dest.x,dest.y,pixelSize,pixelSize);
				break;
			case LEFT:
				batch.draw(tailRegion3,dest.x,dest.y,pixelSize,pixelSize);
				break;
		}
		
		// desenha a maçã
		for (Piece p: apple)
		{
			dest.x = p.pos.x * pixelSize;
			dest.y = p.pos.y * pixelSize;
			batch.draw(appleRegion, dest.x, dest.y, pixelSize, pixelSize);
		}
		
		// desenha a cabeça
		dest.x = body.lastElement().pos.x * pixelSize;
		dest.y = body.lastElement().pos.y * pixelSize;
		
		switch (body.lastElement().dir)
		{
			case UP:// se está indo pra cima
				batch.draw(headRegion1, dest.x, dest.y, pixelSize, pixelSize);
				break;
			case DOWN:// se está indo para baixo
				batch.draw(headRegion3, dest.x, dest.y, pixelSize, pixelSize);
				break;
			case RIGHT:// se está indo para direita
				batch.draw(headRegion2, dest.x, dest.y, pixelSize, pixelSize);
				break;
			case LEFT:// se está indo para esquerda
				batch.draw(headRegion0, dest.x, dest.y, pixelSize, pixelSize);
				break;
		}
	}
	
	// se é uma parte com comida
	public boolean isEaten ( Piece p ) {
		for (Vector2 q: eatenPos)
		{
			if (q.x == p.pos.x && q.y == p.pos.y)
				return true;
		}
		
		return false;
	}
	
	public void drawBody ( SpriteBatch batch ) {
		int bef, mid, aft; 
		for (int i = 1; i < body.size() - 1; i++)
		{
			bef = body.get(i-1).dir;
			mid = body.get(i).dir;
			aft = body.get(i+1).dir;
			dest.set(body.get(i).pos.x * pixelSize, body.get(i).pos.y * pixelSize);
			/*
			 * O da frente não pode estar na direção contraria ao de trás
			 */
			if ((bef == LEFT && mid == LEFT && aft == LEFT) ||
			    (bef == LEFT && mid == LEFT && aft == UP) ||
			    (bef == LEFT && mid == LEFT && aft == DOWN))
			{
				if (isEaten(body.get(i)) == false)
				{
					batch.draw(lineRegion1,dest.x,dest.y,pixelSize,pixelSize);
				}
				else
					batch.draw(eatenLineRegion1,dest.x,dest.y,pixelSize,pixelSize);
			}
			//else if (bef == LEFT && mid == RIGHT && aft == LEFT) //não pode
			else if ((bef == LEFT && mid == DOWN && aft == LEFT) || 
			         (bef == LEFT && mid == DOWN && aft == RIGHT) ||
			         (bef == LEFT && mid == DOWN && aft == DOWN))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion3,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenCornerRegion3,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == LEFT && mid == UP && aft == LEFT) || 
			         (bef == LEFT && mid == UP && aft == RIGHT) ||
			         (bef == LEFT && mid == UP && aft == UP))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion2,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenCornerRegion2,dest.x,dest.y,pixelSize,pixelSize);
			}
			//else if ((bef == RIGHT && mid == LEFT && aft == LEFT)//não pode
			else if ((bef == RIGHT && mid == RIGHT && aft == DOWN) || 
			         (bef == RIGHT && mid == RIGHT && aft == RIGHT) ||
			         (bef == RIGHT && mid == RIGHT && aft == UP))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(lineRegion1,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenLineRegion1,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == RIGHT && mid == DOWN && aft == DOWN) || 
			         (bef == RIGHT && mid == DOWN && aft == RIGHT) ||
			         (bef == RIGHT && mid == DOWN && aft == LEFT))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion0,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenCornerRegion0,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == RIGHT && mid == UP && aft == UP) || 
			         (bef == RIGHT && mid == UP && aft == RIGHT) ||
			         (bef == RIGHT && mid == UP && aft == LEFT))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion1,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenCornerRegion1,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == UP && mid == UP && aft == UP) || 
			         (bef == UP && mid == UP && aft == RIGHT) ||
			         (bef == UP && mid == UP && aft == LEFT))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(lineRegion0,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenLineRegion0,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == UP && mid == RIGHT && aft == UP) || 
			         (bef == UP && mid == RIGHT && aft == RIGHT) ||
			         (bef == UP && mid == RIGHT && aft == DOWN))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion3,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(cornerRegion3,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == UP && mid == LEFT && aft == UP) || 
			         (bef == UP && mid == LEFT && aft == DOWN) ||
			         (bef == UP && mid == LEFT && aft == LEFT))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion0,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenCornerRegion0,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == DOWN && mid == LEFT && aft == UP) || 
			         (bef == DOWN && mid == LEFT && aft == DOWN) ||
			         (bef == DOWN && mid == LEFT && aft == LEFT))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion1,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenCornerRegion1,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == DOWN && mid == RIGHT && aft == UP) || 
			         (bef == DOWN && mid == RIGHT && aft == RIGHT) ||
			         (bef == DOWN && mid == RIGHT && aft == DOWN))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(cornerRegion2,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenCornerRegion2,dest.x,dest.y,pixelSize,pixelSize);
			}
			else if ((bef == DOWN && mid == DOWN && aft == LEFT) || 
			         (bef == DOWN && mid == DOWN && aft == RIGHT) ||
			         (bef == DOWN && mid == DOWN && aft == DOWN))
			{
				if (isEaten(body.get(i)) == false)
					batch.draw(lineRegion0,dest.x,dest.y,pixelSize,pixelSize);
				else
					batch.draw(eatenLineRegion0,dest.x,dest.y,pixelSize,pixelSize);
			}
			//else if ((bef == DOWN && mid == UP && aft == UP)//não pode 
		}
	}
	
	
	public void startGame (  ) {
		//tamanho_anterior = tamanho; // para o hud
		//Reinicie o jogo
		//tamanho = 5;
		eatSnake = false;
		vel.x = 0;
		vel.y = 0;
		
		for (int i = 0; i < keys.length; i++)
			keys[i] = false;
		
		// reinicializando as peças
		// inicializando a parte A - a cabeça
		body.clear();
		for (int i = 5; i > 0; i--)
		{
			body.add(new Piece((int)mapWidth/2 + mapStartW, (int)mapHeight/2 + i + mapStartH, UP));
		}
		
		// inicializando as coordenadas da maçã.
		//posiciona_maca();
		for (Piece p: apple)
			setApplePos(p);
	}
	
	public void setApplePos ( Piece apple ) {
		boolean repeat = false;
		int[] applePos = {0,0};
		Rect appleRect = new Rect(0,0,(int)pixelSize,(int)pixelSize);
		do
		{
			// escolhe aleatoriamente as coordenadas
			applePos[0] = (int)(Math.random() * (mapWidth) + mapStartW + 1);
			applePos[1] = (int)(Math.random() * (mapHeight) + mapStartH + 1);
	
			repeat = false;
			
			
			if (applePos[0] >= mapWidth + mapStartW - 1)
				applePos[0] = (int)(mapWidth + mapStartW - 2);
			
			if (applePos[1] >= mapHeight + mapStartH - 1)
				applePos[1] = (int)(mapHeight + mapStartH - 2);
			
			for (Piece p: body)
			{
				// verifica em todas as peças se as coordenadas delas
				// são iguais as novas coordenadas da maçã.
				if ((p.pos.x == applePos[0]) && (p.pos.y == applePos[1]))
				{
					appleRect.x = (int)(applePos[0]*pixelSize);
					appleRect.y = (int)(applePos[1]*pixelSize);
					
					
					
					// Se forem iguais então pare o loop e
					// repita o procedimento para escolher outra coordenada para a maçã.
					repeat = true;
					break;
				}
			}
			
			for (Rect r: excludedArea)
				if (r.boundingbox(appleRect))
				{
					repeat = true;
					break;
				}
		// enquanto for para repetir continue escolhendo outra coordenada para a maçã.
		} while (repeat);

		apple.pos.x = applePos[0];
		apple.pos.y = applePos[1];
	}
	
	@Override
	public void resize(int width, int height) {
		gui.resize(width,height);
		viewport.update(width,height);
	}
	
	public void keyboard ( int keycode, boolean state ) {
		if (state)
		{
			switch (keycode)
			{
				case Keys.UP:
					keys[UP]=true;
					keys[DOWN]=false;
					keys[RIGHT]=false;
					keys[LEFT]=false;
					break;
				case Keys.DOWN:
					keys[UP]=false;
					keys[DOWN]=true;
					keys[RIGHT]=false;
					keys[LEFT]=false;
					break;
				case Keys.RIGHT:
					keys[UP]=false;
					keys[DOWN]=false;
					keys[RIGHT]=true;
					keys[LEFT]=false;
					break;
				case Keys.LEFT:
					keys[UP]=false;
					keys[DOWN]=false;
					keys[RIGHT]=false;
					keys[LEFT]=true;
					break;
				case Keys.ENTER:
					setApplePos(apple.get(0));
					break;
			}
		}
	}
	 
	

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		keyboard(keycode,true);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK || keycode == Keys.ESCAPE)
			GameState.changeTo("levelSelector");
		keyboard(keycode,false);
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}

	public void reload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onChange() {
		// TODO Auto-generated method stub
		
	}

}
