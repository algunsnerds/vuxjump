package com.game.vuxjump;

import java.util.HashMap;
import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.game.vuxjump.gui.ConfigurationMenu;
import com.game.vuxjump.gui.LevelSelectorScreen;
import com.game.vuxjump.gui.MainMenuScreen;
import com.game.vuxjump.hud.LevelHUD;
import com.game.vuxjump.minigame.Snake;
import com.game.vuxjump.screen.GameScreen;
import com.game.vuxjump.screen.HorizontalJumpLevel;
import com.game.vuxjump.screen.VerticalJumpLevel;


public class GameState {
	private static GameState singleton = null;
	private GameScreen currScreen;
	private String currName = "null";
	private HashMap<String, GameScreen> screens;
	private VuxJump game;
	private SpriteBatch batch;
	private String oriValue = "landscape";
	private OrientationChanger orientation;
	private boolean isReload = false;
	
	private GameState (  ) {
		currScreen = null;
		batch = null;
		screens = new HashMap<String, GameScreen>();
		orientation = null;
	}
	
	public synchronized static GameState instance (){
		if (singleton == null)
			singleton = new GameState();
		
		return singleton;
	}

	public void dispose (  ) {
		for (Entry<String,GameScreen> screen: screens.entrySet())
			if (screen.getValue() != null)
				screen.getValue().dispose();
	}

	public void setOrientationChanger ( OrientationChanger oc ) {
		orientation = oc;
	}
	
	public OrientationChanger getOrientationChanger (  ) {
		return orientation;
	}
	
	protected void setOriValue (String ori ) {
		oriValue = ori;
	}
	
	protected String getOriValue() {
		return oriValue;
	}
	
	public static String getOrientationValue (  ) {
		return instance().getOriValue();
	}
	
	public static void setOrientation ( String ori ) {
		if (instance().getOrientationChanger() != null)
		{
			instance().getOrientationChanger().changeTo(ori);
			instance().setOriValue(ori);
		}
		else
		{
			instance().setOriValue("landscape");
			Gdx.app.log("GameState", "Impossivel trocar de orientação: só funciona no Android");
		}
	}
	
	public String getCurrName (  ) {
		return currName;
	}
	
	private void setCurrName ( String name ) {
		currName = name;
	}
	
	public GameScreen getCurrScreen (  ) {
		return currScreen;
	}

	public GameScreen getScreen ( String name ) {
		return screens.get(name);
	}

	private void setCurrScreen (String name) {
		if(getScreen(name) != null)
		{
			//executa o onChange do último screen
			if (currScreen != null)
				currScreen.onChange();
			currScreen = getScreen(name);
			
			game.setScreen(currScreen);
			//reload();
		}
	}
	
	public boolean hasScreen ( String name ) {
		return (screens.containsKey(name));
	}
	
	public void addScreen(String nameScreen, GameScreen currScreen){
		if (hasScreen(nameScreen) == false)
			screens.put(nameScreen, currScreen);
	}
	
	public GameScreen remScreen ( String name ) {
		if (screens.containsKey(name))
		{
			return screens.remove(name);
		}
		
		return null;
	}
	
	public SpriteBatch getBatch (  ) {
		return batch;
	}
	
	public void setBatch ( SpriteBatch batch ) {
		this.batch = batch;
	}
	
	public VuxJump getGame (  ) {
		return game;
	}
	
	public void setGame ( VuxJump game ) {
		this.game = game;
	}
	
	
	// destroi um screen especifico
	public static void destroy ( String screen ) {
		GameScreen scr = instance().remScreen(screen);
		if (scr != null)
			scr.dispose();
		LevelHUD.dispose();
	}
	
	public void setReloadOnResume(boolean b) {
		isReload = b;
	}
	
	public void reload (  ) {
		/*
		if (isReload)
			currScreen.reload();
		*/
	}
	
	public static void changeTo ( String screen ) {
		GameScreen curr = instance().getScreen(screen);
		
		if (screen.equals("mainMenu"))
		{
			setOrientation("landscape");
			if (curr == null)
			{
				curr = new MainMenuScreen();
				instance().addScreen(screen, curr);
			}
		}
		else if (screen.equals("greenLevel"))
		{
			setOrientation("landscape");
			if (curr == null)
			{
				String[] paths = {"groups/greenGroup2.json","groups/greenGroup3.json","groups/greenGroup2.json","groups/greenGroup3.json"};
				curr = new HorizontalJumpLevel("greenLevel/greenBackground.png","greenLevel/greenPlatforms.png",paths, true);
				
				((HorizontalJumpLevel)curr).setTreakChance(0.3f);
				
				instance().addScreen(screen, curr);
			}
		}
		else if (screen.equals("iceLevel"))
		{
			setOrientation("portrait");
			if (curr == null)
			{
				String[] paths = {"groups/iceGroup1.json","groups/iceGroup2.json","groups/iceGroup2.json","groups/iceGroup1.json"};//"groups/iceGroup3.json"};
				curr = (GameScreen)new VerticalJumpLevel("iceLevel/iceBackground.png","iceLevel/icePlatforms.png",paths, false);
				instance().addScreen(screen, curr);
			}
			
			//((IceLevelScreen)curr).getCamera().rotate(90);
		}
		else if (screen.equals("futureLevel"))
		{
			setOrientation("landscape");
			if (curr == null)
			{
				String[] paths = {"groups/futureGroup1.json","groups/futureGroup2.json","groups/futureGroup1.json","groups/futureGroup2.json"};
				curr = (GameScreen)new HorizontalJumpLevel("futureLevel/futureBackground.png","futureLevel/futurePlatforms.png",paths, 1024,1024);
				
				((HorizontalJumpLevel)curr).setTreakChance(0.4f);
				
				instance().addScreen(screen, curr);
			}
		}
		else if (screen.equals("configMenu"))
		{
			setOrientation("landscape");
			if (curr == null)
			{
				curr = new ConfigurationMenu();
				instance().addScreen(screen, curr);
			}
		}
		else if (screen.equals("levelSelector"))
		{
			setOrientation("landscape");
			if (curr == null)
			{
				curr = new LevelSelectorScreen();
				instance().addScreen(screen, curr);
			}
		}
		else if (screen.equals("snake"))
		{
			setOrientation("portrait");
			if (curr == null)
			{
				curr = new Snake();
				instance().addScreen(screen, curr);
			}
		}
		else
		{
			Gdx.app.log("GameState", "Não existe Screen: \""+screen+"\"");
		}
		
		if (curr != null)
		{
			instance().setCurrScreen(screen);
			instance().setCurrName(screen);
		}
			
	}

	
	
	
}
