package com.game.vuxjump;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
// em qual classe tu implementa a classe inputprocessor? no levelscreen
public class StateMachine {
	protected Vector2 pos;
	private int state;
	
	public StateMachine (  ) {
		state = 0;
		pos = new Vector2();
	}
	
	public void setPosition ( Vector2 p ) {
		pos.x = p.x;
		pos.y = p.y;
	}
	
	public void setPosition ( float x, float y ) {
		pos.x = x;
		pos.y = y;
	}
	
	public Vector2 getPosition (  ) {
		return pos;
	}
	
	public void setState ( int s ) {
		state = s;
	}
	
	public int getState (  ) {
		return state;
	}
	
	public int update (  ) {
		return getState();
	}
	
	public void draw ( SpriteBatch batch, OrthographicCamera camera ) {
		
	}
}
