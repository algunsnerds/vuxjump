package com.game.vuxjump;

import com.badlogic.gdx.Game;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class VuxJump extends Game {
	public AssetManager assetManager;
	public SpriteBatch batch;
	
	public VuxJump ( OrientationChanger oc ) {
		GameState.instance().setOrientationChanger(oc);
	}
	
	@Override
	public void create() {
		batch = new SpriteBatch();
		GameState.instance().setBatch(batch);
		GameState.instance().setGame(this);
		
		if (GameState.instance().getCurrScreen() == null)
		{
			//GameState.changeTo("mainMenu");
			GameState.changeTo("greenLevel");
			//GameState.changeTo("snake");
		}
	}
	
	@Override
	public void resume (  ) {
		super.resume();
		GameState.instance().setGame(this);
		setScreen(GameState.instance().getCurrScreen());
	}
	
	@Override
	public void dispose() {
		super.dispose();
		GameState.instance().dispose();
		batch.dispose();
	}
}
