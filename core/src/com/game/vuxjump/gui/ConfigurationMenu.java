package com.game.vuxjump.gui;

import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.game.vuxjump.GameState;
import com.game.vuxjump.StringActor;
import com.game.vuxjump.VuxJump;
import com.game.vuxjump.screen.GameScreen;

public class ConfigurationMenu implements GameScreen {
	private VuxJump game;
	private Button backButton;
	private Skin skinBackButton;
	private Skin skinButtons;
	private Texture backTexture;
	private Texture buttonState[] = new Texture[2];
	private Stage stage = new Stage();
	private Button[] buttons;
	private Preferences prefs;
	private StringActor strings[] = new StringActor[6];
	private OrthographicCamera camera;
	private boolean inited = false;
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		if(inited)
			return;
		
		inited = true;
		prefs = Gdx.app.getPreferences("configs");
		
		camera = new OrthographicCamera(1024,600);
		camera.setToOrtho(false, 1024,600);
		stage.setViewport(new StretchViewport(1024,600,camera));
		
		buttons = new Button[5];
		backTexture = new Texture(Gdx.files.internal("gui/backButton.png"));
		
		buttonState[0] = new Texture(Gdx.files.internal("gui/off.png"));
		buttonState[1] = new Texture(Gdx.files.internal("gui/on.png"));
		
		
		skinBackButton = new Skin();
		skinButtons = new Skin();
		
		skinBackButton.add("backTexture", backTexture);
		
		skinButtons.add("off", buttonState[0]);
		skinButtons.add("on", buttonState[1]);
		
		ButtonStyle buttonBackStyle = new ButtonStyle();
		buttonBackStyle.up = skinBackButton.newDrawable("backTexture", Color.WHITE);
		buttonBackStyle.down = skinBackButton.newDrawable("backTexture", Color.DARK_GRAY);
		buttonBackStyle.over = skinBackButton.newDrawable("backTexture", Color.LIGHT_GRAY);
		buttonBackStyle.checked = skinBackButton.newDrawable("backTexture", Color.DARK_GRAY);
		
		backButton = new Button(buttonBackStyle);
		backButton.setWidth(100f);
		backButton.setHeight(100f);
		backButton.setPosition(camera.viewportWidth - backButton.getWidth() - backButton.getWidth() * 0.30f, camera.viewportHeight - backButton.getHeight() - backButton.getHeight() * 0.30f);
		
		backButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				prefs.flush();
				backButton.setChecked(false);
				GameState.changeTo("mainMenu");
			}
		});
		
		//stage.addActor(backButton);

		ButtonStyle buttonActiveStyle = new ButtonStyle();
		buttonActiveStyle.up = skinButtons.newDrawable("off", Color.WHITE);
		buttonActiveStyle.down = skinButtons.newDrawable("on", Color.WHITE);
		buttonActiveStyle.checked = skinButtons.newDrawable("on", Color.WHITE);
		//buttonActiveStyle.over = skinButtons.newDrawable("on", Color.LIGHT_GRAY);
		// cara coloca uns coments aqui
		buttons[0] = new Button(buttonActiveStyle);
		buttons[0].setPosition(900 - buttons[0].getWidth(),500 - buttons[0].getHeight());
		
		buttons[0].addListener(new ClickListener(){ @Override 
			public void clicked(InputEvent event, float x, float y) {
				buttons[0].setChecked(buttons[0].isChecked());
			}});
		
		buttons[1] = new Button(buttonActiveStyle);
		buttons[1].setPosition(900 - buttons[1].getWidth(),400 - buttons[1].getHeight());
		buttons[1].addListener(new ClickListener(){@Override
			public void clicked(InputEvent event, float x, float y) {
				buttons[1].setChecked(buttons[1].isChecked());				
			}});
		
		buttons[2] = new Button(buttonActiveStyle);
		buttons[2].setPosition(900 - buttons[0].getWidth(),300 - buttons[0].getHeight());
		buttons[2].setChecked(prefs.getBoolean("vsync"));
		buttons[2].addListener(new ClickListener(){@Override
			public void clicked(InputEvent event, float x, float y) {
				if(!prefs.contains("vsync")){
					prefs.putBoolean("vsync", false);
				}
				
				if(buttons[2].isChecked()){
					prefs.putBoolean("vsync", true);
				}else{
					prefs.putBoolean("vsync", false);
				}
				
				buttons[2].setChecked(prefs.getBoolean("vsync"));
				
				prefs.flush();
				
			}});
		
		buttons[3] = new Button(buttonActiveStyle);
		buttons[3].setPosition(900 - buttons[3].getWidth(),200 - buttons[3].getHeight());
		buttons[3].setChecked(prefs.getBoolean("showFPS"));
		buttons[3].addListener(new ClickListener(){@Override
			public void clicked(InputEvent event, float x, float y) {
				if(!prefs.contains("showFPS")){
					prefs.putBoolean("showFPS", false);
				}
				
				if(buttons[3].isChecked()){
					prefs.putBoolean("showFPS", true);
				}else{
					prefs.putBoolean("showFPS", false);
				}
				
				buttons[3].setChecked(prefs.getBoolean("showFPS"));
				prefs.flush();
			}});
		
		buttons[4] = new Button(buttonActiveStyle);
		buttons[4].setPosition(900 - buttons[4].getWidth(),100 - buttons[4].getHeight());
		
		buttons[4].addListener(new ClickListener(){@Override
			public void clicked(InputEvent event, float x, float y) {
				buttons[4].setChecked(false);
				prefs.putInteger("0", 0);
				prefs.putInteger("1", 0);
				prefs.putInteger("2", 0);
				prefs.putInteger("3", 0);
				prefs.putInteger("4", 0);			
				prefs.flush();
			}});
			
		strings[0] = new StringActor("font50", "font50.json", "font50.png", "Configuration Menu");
		strings[0].flip(false, true);
		strings[0].setPosition(Gdx.graphics.getWidth()/2 - strings[0].getWidth()/2, Gdx.graphics.getHeight() - strings[0].getHeight() - 50f);

		strings[1] = new StringActor("font50", "font50.json", "font50.png", "Config 1");
		strings[1].flip(false, true);
		
		strings[2] = new StringActor("font50", "font50.json", "font50.png", "Config 2");
		strings[2].flip(false, true);
		
		strings[3] = new StringActor("font50", "font50.json", "font50.png", "Show FPS");
		strings[3].flip(false, true);
		
		strings[4] = new StringActor("font50", "font50.json", "font50.png", "Enable VSync");
		strings[4].flip(false, true);

		strings[5] = new StringActor("font50", "font50.json", "font50.png", "Reset Score");
		strings[5].flip(false, true);

		Table table = new Table();
		//table1.add(strings[0]).center().right();
		//table1.add(backButton).height(64f).width(64f).top().right();
		table.setFillParent(true);
		table.add(strings[1]).expandX().fillX();
		table.add(buttons[0]).expandX();
		table.row();
		table.add(strings[2]).expandX().fillX();
		table.add(buttons[1]).expandX();
		table.row();
		table.add(strings[3]).expandX().fillX();
		table.add(buttons[2]).expandX();
		table.row();
		table.add(strings[4]).expandX().fillX();
		table.add(buttons[3]).expandX();		
		table.row();
		table.add(strings[5]).expandX().fillX();
		table.add(buttons[4]).expandX();
		
		table.setDebug(true);
		
		stage.addActor(strings[0]);
		stage.addActor(backButton);
		stage.addActor(table);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();	      
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	public void reload() {
		inited = false;
	}
	
	@Override
	public void dispose() {
		for(StringActor i : strings){
			i.dispose();
		}
		backTexture.dispose();

		for(Texture t: buttonState){
			t.dispose();
		}
		prefs.flush();
		skinBackButton.dispose();
		skinButtons.dispose();
		stage.dispose();
		
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public VuxJump getGame() {
		return game;
	}	
	
	void setGame(VuxJump g){
		game = g;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onChange() {
		// TODO Auto-generated method stub
		
	}

}
