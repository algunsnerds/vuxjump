package com.game.vuxjump.gui;

import java.util.Vector;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.game.vuxjump.GameState;
import com.game.vuxjump.Rect;
import com.game.vuxjump.Writer;
import com.game.vuxjump.screen.GameScreen;

public class LevelGui {
	private OrthographicCamera camera;
	
	// For Buttons
	private Skin skin = new Skin();
	private Stage stage = new Stage();
	
	// Pause Button
	private Button pauseButton;
	private Texture pauseTexture;

	// Resume Button
	private Button playButton;
	private Texture playTexture;
	
	// Back Button
	private Button backButton;
	private Texture backTexture;
	
	/*
	 * Image é uma subclasse de Actor, por isso pode-se usar em Stage
	 */
	// setas
	private Image leftArrow;
	private Image rightArrow;
	private static Texture arrowTexture = null;
	
	
	private boolean hasArrows = false;
	private static Vector<TextureRegion> arrows = null;
	
	private Preferences prefs = Gdx.app.getPreferences("configs");
	
	private EndLevelWindow endWindow;//falta desalocar memoria
	
	private boolean paused = false;
	
	public LevelGui ( GameScreen screen ){
		Gdx.input.setCatchBackKey(true);
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(stage);
		inputMultiplexer.addProcessor(new GestureDetector(screen));
		inputMultiplexer.addProcessor((InputProcessor)screen);

		Gdx.input.setInputProcessor(inputMultiplexer);
		
		hasArrows = false;
		init(screen);
	}
	
	void init ( GameScreen screen ) {
		if (GameState.getOrientationValue().equals("portrait"))
		{
			camera = new OrthographicCamera(600,1024);
			camera.setToOrtho(false, 600,1024);
			stage.setViewport(new StretchViewport(600,1024, camera));
		}
		else
		{
			camera = new OrthographicCamera(1024,600);
			camera.setToOrtho(false, 1024,600);
			stage.setViewport(new StretchViewport(1024, 600, camera));
		}
		Gdx.graphics.setVSync(prefs.getBoolean("vsync"));
		
		pauseTexture = new Texture(Gdx.files.internal("gui/pauseButton.png"));
		
		skin.add("pauseTexture", pauseTexture);
		
		ButtonStyle buttonPauseStyle = new ButtonStyle();
		buttonPauseStyle.up = skin.newDrawable("pauseTexture", Color.WHITE);
		buttonPauseStyle.down = skin.newDrawable("pauseTexture", Color.DARK_GRAY);
		buttonPauseStyle.over = skin.newDrawable("pauseTexture", Color.LIGHT_GRAY);
		buttonPauseStyle.checked = skin.newDrawable("pauseTexture", Color.DARK_GRAY);
		
		pauseButton = new Button(buttonPauseStyle);
		pauseButton.setWidth(64f);
		pauseButton.setHeight(64f);
		pauseButton.setPosition(camera.viewportWidth - pauseButton.getWidth(), camera.viewportHeight - pauseButton.getHeight());
		
		pauseButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				setPaused(true);
			}
		});
		
		stage.addActor(pauseButton);

		playTexture = new Texture(Gdx.files.internal("gui/playButton.png"));
		
		skin.add("texturePlay", playTexture);
		
		ButtonStyle buttonPlayStyle = new ButtonStyle();
		buttonPlayStyle.up = skin.newDrawable("texturePlay", Color.WHITE);
		buttonPlayStyle.over = skin.newDrawable("texturePlay", Color.LIGHT_GRAY);
		buttonPlayStyle.down = skin.newDrawable("texturePlay", Color.DARK_GRAY);
		buttonPlayStyle.checked = skin.newDrawable("texturePlay", Color.LIGHT_GRAY);
		
		playButton = new Button(buttonPlayStyle);
		playButton.setPosition( camera.viewportWidth / 2 - playButton.getWidth() / 2 , camera.viewportHeight / 2 - playButton.getHeight() / 2);
		playButton.setVisible(false);
		playButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				setPaused(false);
			}
		});
		
		stage.addActor(playButton);			

		backTexture = new Texture(Gdx.files.internal("gui/backButton.png"));
		
		skin.add("backTexture", backTexture);
		
		ButtonStyle buttonBackStyle = new ButtonStyle();
		buttonBackStyle.up = skin.newDrawable("backTexture", Color.WHITE);
		buttonBackStyle.down = skin.newDrawable("backTexture", Color.DARK_GRAY);
		buttonBackStyle.over = skin.newDrawable("backTexture", Color.LIGHT_GRAY);
		buttonBackStyle.checked = skin.newDrawable("backTexture", Color.DARK_GRAY);
		
		backButton = new Button(buttonBackStyle);
		backButton.setWidth(64f);
		backButton.setHeight(64f);
		backButton.setPosition(camera.viewportWidth/2 - backButton.getWidth()/2, camera.viewportHeight/2 - (backButton.getHeight()*3));
		backButton.setVisible(false);
		backButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				setPaused(false);
				GameState.changeTo("levelSelector");
			}
		});
		
		stage.addActor(backButton);
		
		
		
		
		if (arrowTexture == null)
			arrowTexture = new Texture(Gdx.files.internal("gui/arrowButton2.png"));
		
		
		TextureRegion leftRegion = new TextureRegion(arrowTexture);
		TextureRegion rightRegion = new TextureRegion(leftRegion);
		leftRegion.flip(true,false);;
		
		// as imagens das setas
		leftArrow = new Image(leftRegion);
		rightArrow = new Image(rightRegion);
		
		if (GameState.getOrientationValue().equals("portrait"))
		{
			leftArrow.setPosition(0,1024/2);
			rightArrow.setPosition(600 - rightRegion.getRegionWidth(), 1024/2);
		}
		else
		{
			leftArrow.setPosition(0, 0);
			rightArrow.setPosition(1024 - rightRegion.getRegionWidth(), 0);
		}
		
		stage.addActor(leftArrow);
		stage.addActor(rightArrow);
		
		leftArrow.setVisible(false);
		rightArrow.setVisible(false);
		
		
		endWindow = new EndLevelWindow(camera, 0,0);
		
		stage.addActor(endWindow);
		
		Writer.instance().loadFont("font50", "font50.json", "font50.png");
	}
	
	public void setArrows ( boolean a ) {
		hasArrows = a;
		leftArrow.setVisible(a);
		rightArrow.setVisible(a);
	}
	
	private void setPaused(boolean b) {
		if(b){
			playButton.setVisible(true);
			playButton.setChecked(false);
			backButton.setVisible(true);
			backButton.setChecked(false);
			pauseButton.setVisible(false);
			pauseButton.setChecked(false);
		}else{
			playButton.setVisible(false);
			playButton.setChecked(false);
			backButton.setVisible(false);
			backButton.setChecked(false);
			pauseButton.setVisible(true);
			pauseButton.setChecked(false);
			//
			if (hasArrows)
			{
				leftArrow.setVisible(true);
				rightArrow.setVisible(true);
			}
		}
		paused = b;
	}
	
	public void resize ( int w, int h ){
		stage.getViewport().update(w,h);
	}
	/*
	class MyTextInputListener implements TextInputListener {
		
		@Override
		public void input ( String text ) {
			
		}

		@Override
		public void canceled() {
			// TODO Auto-generated method stub
			
		}
	}
	*/
	
	int once = 0;
	public void draw (  ){
		SpriteBatch batch = GameState.instance().getBatch();
		
		batch.begin();
			if(isPaused())
			{
				batch.setColor(1,1,1,0.5f);
			}
			else
			{
				batch.setColor(1,1,1,1f);
			}
		batch.end();
		
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	public void dispose(){
		backTexture.dispose();
		pauseTexture.dispose();
		playTexture.dispose();
		endWindow.dispose();
		if (arrowTexture != null)
		{
			arrowTexture.dispose();
			arrowTexture = null;
		}
		skin.dispose();
		stage.dispose();
	}
	
	public boolean isPaused(){
		return paused;
	}

	public Rect getPauseRect() {
		return new Rect((int)pauseButton.getX(),(int)pauseButton.getY(),(int)pauseButton.getWidth(),(int)pauseButton.getHeight());
	}
	
}
