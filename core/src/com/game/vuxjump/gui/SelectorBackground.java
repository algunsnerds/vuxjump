package com.game.vuxjump.gui;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.game.vuxjump.ScrolledBackground;

public class SelectorBackground {
	private Viewport viewport;
	private OrthographicCamera camera;
	private ScrolledBackground background;
	private Vector2 vel;
	private Vector2 pos;
	
	public SelectorBackground ( String path, float vx, float vy, int w, int h ) {
		vel = new Vector2(vx,vy);
		pos = new Vector2();
		camera = new OrthographicCamera(1024,600);
		camera.setToOrtho(true,1024,600);
		viewport = new StretchViewport(1024,600,camera);
		background = new ScrolledBackground(camera,path, w,h);
	}
	
	public void dispose (  ) {
		background.dispose();
	}
	
	public void reset (  ) {
		//camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);
		pos.set(0,0);
	}
	
	public void draw ( SpriteBatch batch ) {
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			background.drawZero(batch,camera);
		batch.end();
	}
	
	public void updateViewport ( int w, int h ) {
		viewport.update(w,h);
	}
	
	public void update (  ) {
		pos.x += vel.x;
		pos.y += vel.y;
		background.setPos(pos);
	}
}
