package com.game.vuxjump.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.game.vuxjump.GameState;

/*
 * Uma janela para mostrar quando a fase acabou ou o player morreu
 */
public class EndLevelWindow extends Actor {
	protected Skin skin;
	protected Button refresh;//com imagem de O?
	protected Button backMenu;//com imagem de X?
	protected ButtonStyle refreshStyle;
	protected ButtonStyle backStyle;
	protected TextureRegion refreshRegion;
	protected TextureRegion backRegion;
	
	public EndLevelWindow ( OrthographicCamera camera, float x, float y ) {
		super();
		//seta a posição da janela, o canto esquerdo inferior é a posição
		//Y aponta para cima
		setPosition(x, y);
		
		skin = new Skin();
		Texture texture = new Texture(Gdx.files.internal("gui/buttonO.png"));
		TextureRegion region = new TextureRegion(texture);
		//region.setRegionHeight(128);
		//region.setRegionWidth(128);
		
		skin.add("refreshRegion", region);
		
		texture = new Texture(Gdx.files.internal("gui/buttonX.png"));
		region = new TextureRegion(texture);
		//region.setRegionHeight(128);
		//region.setRegionWidth(128);
		
		skin.add("backRegion", region);
		
		refreshStyle = new ButtonStyle();
		refreshStyle.up = skin.newDrawable("refreshRegion", Color.WHITE);
		refreshStyle.down = skin.newDrawable("refreshRegion", Color.DARK_GRAY);
		refreshStyle.over = skin.newDrawable("refreshRegion", Color.LIGHT_GRAY);
		refreshStyle.checked = skin.newDrawable("refreshRegion", Color.DARK_GRAY);
		
		refresh = new Button(refreshStyle);
		refresh.setWidth(64);;
		refresh.setHeight(64);
		
		backStyle = new ButtonStyle();
		backStyle.up = skin.newDrawable("backRegion", Color.WHITE);
		backStyle.down = skin.newDrawable("backRegion", Color.DARK_GRAY);
		backStyle.over = skin.newDrawable("backRegion", Color.LIGHT_GRAY);
		backStyle.checked = skin.newDrawable("backRegion", Color.DARK_GRAY);
		
		backMenu = new Button(backStyle);
		backMenu.setWidth(64);
		backMenu.setHeight(64);
		
		backMenu.setPosition(x + 64, y);
		refresh.setPosition(backMenu.getX() + backMenu.getWidth()*2, backMenu.getY());
		
		refresh.addListener(new ClickListener() {
			@Override
			public void clicked ( InputEvent event, float x, float y ){
				GameState.changeTo(GameState.instance().getCurrName());
			}
		});
		
		backMenu.addListener(new ClickListener() {
			@Override
			public void clicked ( InputEvent event, float x, float y ) {
				GameState.changeTo(GameState.instance().getCurrName());
			}
		});
	}
	
	
	public void dispose (  ) {
		skin.dispose();
		refreshRegion.getTexture().dispose();
		backRegion.getTexture().dispose();
	}
	
	/*
	@Override
	public void act ( float delta ) {
		
	}
	*/
}
