package com.game.vuxjump.gui;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureAdapter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.game.vuxjump.GameState;
import com.game.vuxjump.screen.GameScreen;

public class LevelSelectorScreen extends GestureAdapter implements GameScreen{
	
	private OrthographicCamera camera;
	private int index = 0;
	private Texture texture;
	private Texture texture2;
	private Texture levelTexture;
	private Stage stage = new Stage();
	private Viewport viewport;
	private Skin skinButton = new Skin();
	private Button backButton;
	private String[] levelNames;
	private Button[] levels;
	private float positionXButton, positionX;
	private boolean inited = false;
	private ButtonStyle buttonStyle, levelButtonStyle;
	private int buttons;
	private SelectorBackground background;
	
	@Override
	public void show() {
		InputMultiplexer inputs = new InputMultiplexer(new GestureDetector(this), stage, this);
		Gdx.input.setInputProcessor(inputs);
		Gdx.input.setCatchBackKey(true);
		if(inited)
		{
			background.reset();
			return;
		}
		inited = true;
		texture = new Texture(Gdx.files.internal("gui/backButton.png"));
		texture2 = new Texture(Gdx.files.internal("gui/playButton.png"));
		
		camera = new OrthographicCamera(1024,600);
		camera.setToOrtho(false, 1024,600);
		viewport = new StretchViewport(1024, 600, camera);
		stage.setViewport(new StretchViewport(1024, 600, camera));
		
		camera.position.x = camera.viewportWidth / 2 + 12;
	
		background = new SelectorBackground("gui/selectorBackground.png", 0.6f,0.6f, 1024,600);
		
		positionX = camera.viewportWidth / 2 - horWidth / 2;
		levelTexture = new Texture(Gdx.files.internal("gui/levelSelector1.png"));
		TextureRegion region = new TextureRegion(levelTexture, 0,0,512,300);
		skinButton.add("levelTexture", region);
		skinButton.add("backTexture", texture);
		skinButton.add("playTexture", texture2);
		
		levelButtonStyle = new ButtonStyle();
		levelButtonStyle.up = skinButton.newDrawable("levelTexture", Color.WHITE);
		levelButtonStyle.down = skinButton.newDrawable("levelTexture", Color.DARK_GRAY);
		levelButtonStyle.over = skinButton.newDrawable("levelTexture", Color.LIGHT_GRAY);
		
		buttonStyle = new ButtonStyle();
		buttonStyle.up = skinButton.newDrawable("backTexture", Color.WHITE);
		buttonStyle.down = skinButton.newDrawable("backTexture", Color.DARK_GRAY);
		buttonStyle.over = skinButton.newDrawable("backTexture", Color.LIGHT_GRAY);

		backButton = new Button(buttonStyle);
		backButton.setWidth(100f);
		backButton.setHeight(100f);
		backButton.setPosition(camera.viewportWidth - backButton.getWidth(), camera.viewportHeight - backButton.getHeight());
		
		backButton.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				stage.addAction(sequence(fadeOut(0.09f),run(new Runnable() {
					@Override
					public void run() {
						backButton.setChecked(false);
						GameState.changeTo("mainMenu");
					}
				}), fadeIn(1.5f)));
			}
		});
		
		backButton.setVisible(true);
		stage.addActor(backButton);
		
		addLevels(new String[]{"greenLevel", "iceLevel", "futureLevel", "snake", "b", "c"});
		
	}
	
	/*
	 * TODO:
	 * Fazer botões de imagem vertical e de horizontal.
	 * Ficaria assim:
	 *           +----+          +----+
	 * +--------+|    |+--------+|    |
	 * |        ||    ||        ||    |
	 * +--------+|    |+--------+|    |
	 *           +----+          +----+
	 */
	
	final float horWidth = 512f, horHeight = 300f;
	private void addLevels(final String[] levelNames) {
		this.levelNames = levelNames;
		this.buttons = levelNames.length;
		this.levels = new Button[buttons];
		
		for(int i = 0; i < buttons; i++){
		final int aux = i;
		levels[i] = new Button(levelButtonStyle);
		
		levels[i].setWidth(horWidth);
		levels[i].setHeight(horHeight);
		levels[i].setPosition(camera.viewportWidth / 2 - horWidth / 2 + i * horWidth, camera.viewportHeight / 2 - horHeight / 2);
		levels[i].addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				levels[aux].setChecked(false);
				stage.addAction(sequence(run(new Runnable() {
					@Override
					public void run() {
						positionXButton = Math.round(levels[aux].getX());
						positionX = Math.round(positionX);
						if(positionXButton > positionX){
							onLeft();
							return;
						}else if(positionXButton < positionX){
							onRight();
							return;							
						}else if(positionXButton == positionX){
							GameState.changeTo(levelNames[aux]);
						}
					}
				})));
			}
		});
		
		}
		for(Button b : levels)
			stage.addActor(b);

	}

	public void onRight(){
		if((index) > 0){
			index--;
			stage.addAction(sequence(run(new Runnable(){
				@Override
				public void run() {
					for(Button b: levels)
						b.addAction(moveBy(horWidth, 0, 0.5f));
				}
			})));
		}
	}

	public void onLeft(){
		if((index + 1) < buttons){
			index++;
			stage.addAction(sequence(run(new Runnable(){
				@Override
				public void run() {
					for(Button b: levels)
						b.addAction(moveBy(-horWidth, 0, 0.5f));
				}
			})));
		}
	}
	
	SpriteBatch batch = GameState.instance().getBatch();
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.setColor(1f,1f,1f,1f);
		background.update();

		background.draw(batch);
	    stage.act(Gdx.graphics.getDeltaTime());
	    stage.draw();
		
	}
	
	@Override
	public void resize(int width, int height) {
		viewport.update(width,height);
		stage.getViewport().update(width, height);	
		background.updateViewport(width,height);
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		skinButton.dispose();
		stage.dispose();
		texture.dispose();
		texture2.dispose();
		levelTexture.dispose();
	}


	@Override
    public boolean fling(float velocityX, float velocityY, int button) {
		boolean ret = false;
		if(velocityX>0){
			onRight();
			ret = true;
		}else{
			onLeft();
			ret = true;
		}
		
		return ret;
	}

	
	@Override
	public boolean keyDown(int keycode) {
		if(keycode == Keys.LEFT){
			onLeft();
		}else if(keycode == Keys.RIGHT){
			onRight();
		}else if(keycode == Keys.ESCAPE){
			GameState.changeTo("mainMenu");
			
		}else if(keycode == Keys.ENTER){
			GameState.changeTo(levelNames[index]);
			
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK)
		{
			GameState.changeTo("mainMenu");
			return true;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public void reload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onChange() {
		// TODO Auto-generated method stub
		
	}

}
	
