package com.game.vuxjump.gui;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.game.vuxjump.GameState;
import com.game.vuxjump.Rect;
import com.game.vuxjump.StaticBackground;
import com.game.vuxjump.Writer;
import com.game.vuxjump.screen.GameScreen;
import com.game.vuxjump.screen.HorizontalJumpLevel;

public class MainMenuScreen implements GameScreen{
	
	private Stage stage = new Stage();
	private Button play;
	private Button configs;
	private Texture texturePlay;
	private StaticBackground logoBG;
	private Texture textureConfig;
	private Skin skin;
	private Viewport viewport;
	private OrthographicCamera camera, stageCamera;

	private HorizontalJumpLevel greenLevel;
	private boolean inited = false;
	
	float time;
	float logow, logoh;
	Vector2 logoPos;
	
	public MainMenuScreen (  ) {
		time = 0;
		logoPos = new Vector2();
	}
	
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		Gdx.input.setCatchBackKey(false);
		if(inited)
		{
			logoPos.x = 233;
			logoPos.y = 30;
			if (Math.random() < 0.3)
			{
				if (Math.random() < 0.5)
					vel.x = 2;
				else
					vel.x = -2;
			}
			else
			{
				//faz parar de mover o logo, mas continua pulando devido a gravidade ativa
				vel.setZero();
			}
			
			stage.unfocusAll();
			return;
		}
		inited = true;
		
		camera = new OrthographicCamera(1024,600);
		camera.setToOrtho(true,1024,600);
		viewport = new StretchViewport(1024,600,camera);
		stageCamera = new OrthographicCamera(1024,600);
		stageCamera.setToOrtho(false);
		stage.setViewport(new StretchViewport(1024,600,stageCamera));
		
		logoBG = new StaticBackground(camera, "gui/logo.png", 512, 325);
		logow = logoBG.getTexture().getWidth();
		logoh = 325;
		
		play = new Button();
		
		skin = new Skin();
		
		texturePlay = new Texture(Gdx.files.internal("gui/playButton.png"));

		
		skin.add("texturePlay", texturePlay);
		
		ButtonStyle buttonPlayStyle = new ButtonStyle();
		buttonPlayStyle.up = skin.newDrawable("texturePlay", Color.WHITE);
		buttonPlayStyle.over = skin.newDrawable("texturePlay", Color.LIGHT_GRAY);
		buttonPlayStyle.down = skin.newDrawable("texturePlay", Color.DARK_GRAY);
		

		play = new Button(buttonPlayStyle);
		play.setPosition( stageCamera.viewportWidth / 2 - play.getWidth() / 2 , stageCamera.viewportHeight / 2 - play.getHeight() / 2 - 128);
		
		play.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				GameState.changeTo("levelSelector");
			}
		});
		
		stage.addActor(play);		
		
		textureConfig = new Texture(Gdx.files.internal("gui/configButton.png"));
		
		skin.add("textureConfig", textureConfig);
		
		ButtonStyle buttonConfigStyle = new ButtonStyle();
		buttonConfigStyle.up = skin.newDrawable("textureConfig", Color.WHITE);
		buttonConfigStyle.over = skin.newDrawable("textureConfig", Color.LIGHT_GRAY);
		buttonConfigStyle.down = skin.newDrawable("textureConfig", Color.DARK_GRAY);
		
		configs = new Button(buttonConfigStyle);
		
		configs.setWidth(64);
		configs.setHeight(64);
		configs.setPosition( stageCamera.viewportWidth / 2 - configs.getWidth() / 2 , stageCamera.viewportHeight / 2 - configs.getHeight() / 2 - 256);
		
		configs.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y){
				GameState.changeTo("configMenu");
			}
		});
		
		stage.addActor(configs);
		
		greenLevel = new HorizontalJumpLevel("greenLevel/greenBackground.png","greenLevel/greenPlatforms.png",
										  new String[]{"groups/greenGroup2.json","groups/greenGroup3.json"}, true);
		greenLevel.init(camera, HorizontalJumpLevel.MENUBG);
		Writer.instance().loadFont("font50", "font50.json", "font50.png");
	}
	
	private float velX = 0.3f;
	private Vector2 vel = new Vector2(2,-3);
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		time += delta;
		/*
		if (time < 0.300)
		{
			logow += 0.2;
			logoh += 0.2;
		}
		else if (time < 0.600)
		{
			logow -= 0.2;
			logoh -= 0.2;
		}
		else
			time = 0;
		*/
		
		camera.position.x += velX;
		camera.update();
		if (camera.position.x + camera.viewportWidth/2 > greenLevel.getCurrGroup().getPosition().x + greenLevel.getCurrGroup().getDim().w)
		{
			velX = -0.3f;
		}
		else if (camera.position.x - camera.viewportWidth/2 < greenLevel.getCurrGroup().getPosition().x)
		{
			velX = 0.3f;
		}
		
		vel.y += 0.1;
		logoPos.y += vel.y;
		if (logoPos.y + logoh >= 400)
		{
			vel.y = -3f;
			logoh = (int)(325*0.9f);
		}
		
		if (logoh < 325)
		{
			logoh++;
		}
		
		logoPos.x += vel.x; 
		if (logoPos.x < 0)
			vel.x = 2;
		else if (logoPos.x + logow > camera.viewportWidth)
			vel.x = -2;
		
		logoBG.update();

		greenLevel.updateLevelBG();

		SpriteBatch batch = GameState.instance().getBatch(); 
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			batch.setColor(1f,1f,1f,1f);
			greenLevel.draw(batch,camera);
			logoBG.draw(batch, camera, logoPos.x, logoPos.y + (325-logoh), logow,logoh);
		batch.end();

    stage.act(Gdx.graphics.getDeltaTime());
    stage.draw();
	}
	
	public void reset (  ) {
		
	}
	
	@Override
	public void resize(int width, int height) {
		viewport.update(width,height);
		stage.getViewport().update(width, height);
	}
	
	
	@Override
	public void dispose() {
		skin.dispose();
		stage.dispose();
		textureConfig.dispose();
		texturePlay.dispose();
		greenLevel.dispose();
		logoBG.dispose();
	}
	
	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK)
		{
			Gdx.app.exit();
			return true;
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void onChange() {
		// TODO Auto-generated method stub
		
	}

	

}
